%% Setup
% This script combines sessions from different tastes to make a combined
% connectivity matrix between the hemispheres
util.clearAll;
% stimuli
outdir = fullfile(util.get_paths().figs,'compare_tastes_corr');
util.mkdir(outdir)

%% load the data and combine betas
% Get the beta tables from matfiles
taste_str = util.str.taste_strings();
beta_tables_taste = ...
    cellfun(@connect.read_matfiles_taste, taste_str, ...
    'UniformOutput', false);

hemis = {{'L', 'R'}, {'L', 'R'}};
[~, correlations, corr_atanh] = cellfun(@(x) connect.correlations(x, hemis),...
    beta_tables_taste, 'UniformOutput', false);
% array of all correlations (the lower triangle part)
[~, corr_tril_vec] = cellfun(@util.math.tril,...
    correlations, 'UniformOutput', false);

%% box plot
colors = [0 0.4470 0.7410; 0.4660 0.6740 0.1880; 0.6350 0.0780 0.1840];
% make arrays for plotting
corr_vec = cat(1, corr_tril_vec{:});
cgroupdata = repelem(categorical(taste_str), cellfun(@height, corr_tril_vec));
t = tiledlayout(1, 1, 'Padding', 'tight');
t.Units = 'centimeters';
t.OuterPosition = [0.25 0.25 5 5];
nexttile;
boxplot(corr_vec',cgroupdata, 'Colors',colors,...
    'Symbol', 'kx', 'Jitter', 1);
util.plot.box_ticks(gca);
%set(gca, 'xtick',1:3, 'xticklabel',{'Cat 1','Cat 2','Cat 3'})
exportgraphics(t,fullfile(outdir, 'boxplot.pdf'), 'ContentType', 'vector')

