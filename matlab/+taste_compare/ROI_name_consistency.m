function [region_order] = ROI_name_consistency(corr_atanh)
%ROI_NAME_CONSISTENCY Get the roi names after checking they are consistent
% across tastes

% row and column order the same
assert(all(cellfun(@(x) isequal(x.Properties.RowNames,...
    x.Properties.VariableNames'), corr_atanh)));
% compare across tastants to be sure you only have 1 region order
row_names = cellfun(@(x) x.Properties.RowNames, corr_atanh, ...
    'UniformOutput', false);
assert(isequal(row_names{:}));
region_order = row_names{1};
end

