function [roi_info] = get_roi_info(roi_names)
%GET_ROI_INFO get the information about hemisphere and ROI name
roi_info = cellfun(@(x) regexp(x,'([L, R])_(\w*)_roi.mat', 'tokens'), ...
    roi_names, 'UniformOutput', false);
roi_info = cellfun(@(x) {x{1}{1}, x{1}{2}}, ...
    roi_info, 'UniformOutput', false);
roi_info = cell2table(cat(1, roi_info{:}), ...
    'VariableNames', {'hemisphere', 'region_name'},...
    'RowNames', roi_names);
% Get identifier strings for variable names and labels
roi_info.identifier_str = cellfun(@(x, y) [x, '-', strrep(y, '_', '-')], ...
    roi_info.hemisphere, roi_info.region_name, 'UniformOutput', false);
roi_info.varname_str = cellfun(@(x) strrep(x, '-', '_'), ...
    roi_info.identifier_str, 'UniformOutput', false);
% Convert to categorical
varNames = roi_info.Properties.VariableNames;
for i = 1:width(roi_info)
    roi_info.(varNames{i}) = categorical(roi_info.(varNames{i}));
end
end

