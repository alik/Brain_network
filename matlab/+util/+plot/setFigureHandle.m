function h = setFigureHandle(h, x_width, y_width, margin,num_axes)
% Sets handle properties according to figure size
%   INPUT:  h: Figure handle

% figure size displayed on screen
real_x_width = x_width*num_axes;
set(h, 'Units','centimeters', 'Position',...
    [0 0 real_x_width+(2 * margin * num_axes) y_width+2*margin])
movegui(h, 'center')

% Set Fonts to Arial
set(findall(h,'-property','FontName'),'FontName','Arial')

% AdditionalParameters For the boxplots
lines = findobj(h, 'type', 'line', 'Tag', 'Median');
set(lines, 'LineWidth', 2);


end
