%% Setup
util.clearAll;
outdir = fullfile(util.get_paths().figs,'graph_centrality');
util.mkdir(outdir)

%% Setup graph
% Read beta values and calculate their correlation
beta_tables_taste = util.load.all_taste_betas();
% first read the full matrix
avg_corr = connect.correlations(beta_tables_taste.combined, ...
    {{'L', 'R'},{'L', 'R'}}).full;

%% Read back modularity from python
% module_csv = fullfile(util.get_paths().python_datadir, 'partition_paper.csv');
% module_idx = readtable(module_csv);
% module_idx = module_idx(1,:);
module_idx = modularity.get_frequent_partition();
% Sort the order of the regions in the modules to match the matrix
[~,idx] = ismember(avg_corr.Properties.VariableNames, ...
    module_idx.Properties.VariableNames);
module_idx = module_idx(:,idx);
% make sure the order of ROIs are the same
assert(isequal(avg_corr.Properties.VariableNames, ...
    module_idx.Properties.VariableNames))
% Get sorted indices
[~, idx] = sort(module_idx.Variables);
avg_corr_sorted = avg_corr(idx,idx);
% only keep the lower half
avg_corr_sorted.Variables = util.math.tril(avg_corr_sorted.Variables);

%% Plot
% correlogram
cur_name = 'modularity';
fh = figure('Name',['Connectivity Matrix: ',...
    cur_name]);
ax = gca;
title(cur_name)
cur_save_name = fullfile(outdir, strjoin({cur_name,'pdf'}, '.'));
plot_textStrings = false;
connect.plot_matrix(avg_corr_sorted, 'tril',plot_textStrings,[0.2, 1]);
util.plot.box_ticks(ax)
util.plot.set_fig_size(fh, [100,100])
util.plot.set_font(ax,9)
exportgraphics(fh, cur_save_name, 'ContentType','vector');

%% Write modules to table
module_idx_sorted = module_idx(:, idx);
%writetable(module_idx_sorted, fullfile(outdir, 'modules.xlsx'));