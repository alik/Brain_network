function [avg_corr, correlations, corr_atanh_T] = correlations(matfile_table,...
    hemisphere, roi_name_variable, var_to_corr)
% CORRELATIONS calculate correlations for a collection of sessions
% INPUT:
%   matfile_table: The table containing beta values to correlate
%   hemisphere: a pair of hemisphere to compare
%   roi_name_variable: default: varname_str, the variable to use for the
%   variable and rowname of the output correlation matrices
%   var_to_corr: the variable used for correlation within each table.
%   options: 'high_low_betas': tastant, 'rinse_Betas': rinse
% OUTPUT:
%   avg_corr: Average of correlations between sessions in full and lower
%   triangular forms
%   correlations: 3D array of all correlations. each session is a 2D plane
%   corr_atanh_T: the table of individual ROI correlations

if ~exist('roi_name_variable','var') || isempty(roi_name_variable)
    roi_name_variable = 'varname_str';
end

if ~exist('var_to_corr','var') || isempty(var_to_corr)
    var_to_corr = 'high_low_betas';
end
assert(length(hemisphere) == 2, 'Compare two hemisphere groups')
% Get example table
example_beta_T = matfile_table{1, 'beta_table'}{1};
all_region_names = example_beta_T.Properties.RowNames;
ROI_names = cellfun(@(hemi_str) ...
    cellstr(example_beta_T{ismember(example_beta_T.hemisphere, hemi_str), ...
    roi_name_variable}), hemisphere, 'UniformOutput', false);
% Get session number
num_sessions = height(matfile_table);
% Number of ROIs in each hemisphere being compared
num_regions = cellfun(@length, ROI_names);
% Put each session's correlation within a 2D slice of a 3D matrix
correlations = zeros([num_regions, num_sessions]);

% combine betas and get the correlations
for i = 1:height(matfile_table)
    cur_T = matfile_table{i, 'beta_table'}{1};
    assert(isequal(cur_T.Properties.RowNames, all_region_names),...
        'Consistent rows');
    % Get the beta values for the given hemispheres
    cur_betas = cellfun(@(hemi_str) ...
        cur_T{ismember(cur_T.hemisphere, hemi_str), var_to_corr}',...
        hemisphere, 'UniformOutput', false);
    cur_correlations = corr(cur_betas{1}, cur_betas{2}, ...
        'rows', 'all');
    correlations(:, :, i) = cur_correlations;
end

% Averge through a tanh transformation
corr_atanh = atanh(correlations);
avg_corr.full = array2table(tanh(mean(corr_atanh, 3, 'omitnan')),...
    'RowNames', ROI_names{1}, 'VariableNames', ROI_names{2});
avg_corr.tril = array2table(util.math.tril(avg_corr.full.Variables, nan),...
    'RowNames', ROI_names{1}, 'VariableNames', ROI_names{2});

% Create a table of individual ROI correlations
corr_atanh_cell = cellfun(@squeeze, num2cell(corr_atanh,3),...
    'UniformOutput', false);
corr_atanh_T = array2table(corr_atanh_cell,...
    'RowNames', ROI_names{1}, 'VariableNames', ROI_names{2});
end