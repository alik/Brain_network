function [split_by_module] = split_by_module(betas_modules,fun)
%SPLIT_BY_MODULE split data by modules and apply a function (fun)
arguments
    betas_modules
    fun = @(x) {x}
end
[t_groups, t_ids] = findgroups(round(betas_modules(:,2)));
assert(isequal(t_ids,[0,1,2]'));
split_by_module = splitapply(fun, betas_modules(:,1), t_groups)';
end

