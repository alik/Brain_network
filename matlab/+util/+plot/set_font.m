function [] = set_font(ax, size)
%FONT set font size and type
arguments
    ax
    size = 8;
end
set(ax, 'FontSize', size, 'fontname', 'Arial')
end

