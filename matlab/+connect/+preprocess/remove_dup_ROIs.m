function [T] = remove_dup_ROIs(T)
%REMOVE_DUP_ROIS remove two duplicated ROIs from beta tables
duplicated_ROIs = {'L_agranularIns_bin_dAICtrim_roi.mat',...
    'filtered_L_dAIC_custom_bin_new_roi.mat'};
T(duplicated_ROIs,:) = [];
end

