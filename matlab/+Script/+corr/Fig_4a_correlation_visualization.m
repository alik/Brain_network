util.clearAll
beta = util.load.beta_table();
outdir = fullfile(util.get_paths().figs,'controls', 'correlation_Viz');

%% Get the Table entries for Left PrCo, left and right PAGs to demonstrate
% high and low correlations of beta series (0.92 vs 0.24)
PRCO_L = beta(beta.region_name == 'PrCo' & beta.hemisphere == 'L',:);
PAG_L = beta(beta.region_name == 'PAG' & beta.hemisphere == 'L',:);
PAG_R = beta(beta.region_name == 'PAG' & beta.hemisphere == 'R',:);
subg_R = beta(beta.region_name == 'subgenual_area25' & beta.hemisphere == 'R',:);
% Assert the order of the sessions is the same for all regions
assert(isequal(PAG_R(:,{'taste', 'old_session_index', 'date_tag'}), ...
    PRCO_L(:,{'taste', 'old_session_index', 'date_tag'}), ...
    PAG_L(:,{'taste', 'old_session_index', 'date_tag'}), ...
    subg_R(:,{'taste', 'old_session_index', 'date_tag'})));

%% Figures using 3D histogram
% R-PAG --> L-PrCO 0.24391 beta correlation
fh = figure(); ax = gca;
hist3([PAG_R.high_low_betas(:),PRCO_L.high_low_betas(:)],...
    'CdataMode','auto', 'Nbins',[40,40])
colormap('hot')
xlim([-3,3])
ylim([-3,3])
xlabel('Right PAG beta')
ylabel('Left PrCo beta')
view(2)
colorbar
util.plot.cosmeticsSave...
    (fh, ax, 2, 2, outdir,...
    'PRCO_L_PAG_R.pdf', 'on','on', true);
exportgraphics(fh,fullfile(outdir,'export_PRCO_L_PAG_R.pdf'),...
    'ContentType','vector')

% R-PAG --> L-PAG 0.92065 correlation
fh = figure(); ax = gca;
colormap('hot')
hist3([PAG_R.high_low_betas(:),PAG_L.high_low_betas(:)],...
    'CdataMode','auto', 'Nbins',[40,40])
xlim([-3,3])
ylim([-3,3])
xlabel('Right PAG beta')
ylabel('Left PAG beta')
view(2)
colorbar
util.plot.cosmeticsSave...
    (fh, ax, 2, 2, outdir,...
    'PAG_L_PAG_R.eps', 'on','on', true);
% The cosmetics save does not save image as vector
exportgraphics(fh,fullfile(outdir,'export_PAG_L_PAG_R.pdf'),...
    'ContentType','vector')

% R-PAG --> R-subG area 25 0.27503
fh = figure(); ax = gca;
colormap('hot')
hist3([PAG_R.high_low_betas(:),subg_R.high_low_betas(:)],...
    'CdataMode','auto', 'Nbins',[40,40])
xlim([-3,3])
ylim([-3,3])
xlabel('Right PAG beta')
ylabel('Right sgACC beta')
view(2)
colorbar
util.plot.cosmeticsSave...
    (fh, ax, 2, 2, outdir,...
    'PAG_R_subG_R.eps', 'on','on', true);
% The cosmetics save does not save image as vector
exportgraphics(fh,fullfile(outdir,'PAG_R_subG_R.pdf'),...
    'ContentType','vector')

%% Get the actual correlation values for the region pairs
combined_betas = util.load.all_taste_betas().combined;
avg_corr_lookup = ...
    connect.correlations(combined_betas, {{'L', 'R'}, {'L', 'R'}},...
    'varname_str');
avg_corr_lookup.full('R_PAG',:)

%% Make line plots
c = util.plot.getColors();

% MEAN
fh = figure(); ax = gca;
hold on
plot(mean(PAG_R.high_low_betas', 2),'Color','k')
plot(mean(PRCO_L.high_low_betas', 2),'Color',[0.5, 0.5, 0.5])
plot(mean(PAG_L.high_low_betas', 2),'Color',[0.8, 0.8, 0]) 
plot(mean(subg_R.high_low_betas', 2),'Color',[0, 0, 0.54])
ylim([-0.2732, 0.2732])
util.plot.cosmeticsSave(fh, ax, 2, 2, outdir,...
    strjoin({'PrCO_PAG_callosal_mean','svg'}, '.'), ...
    'off','on', true);

% RAW
fh = figure(); ax = gca;
hold on
transparency = 0.08;
plot(PAG_R.high_low_betas','Color',[c.magenta,transparency])
plot(PRCO_L.high_low_betas','Color',[([0.5, 0.5, 0.5].*0.5),transparency])
plot(PAG_L.high_low_betas', 'Color',[0.8, 0.8, 0,transparency])
util.plot.cosmeticsSave(fh, ax, 2, 2, outdir,...
    strjoin({'PrCO_PAG_callosal_all','svg'}, '.'), ...
    'off','on', true);