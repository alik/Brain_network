function [] = cosmeticsSave(fh,ax,x_width,y_width,outputFolderLocal,fileName,...
    xtickMinor,ytickMinor,removeDashedLinesForBoxplot)
% cosmeticsSave prepares and saves the figure panel. This is done by
% setting figure and axes properties
% INPUT: fh: The figure handle object
%        ax: axes handle object
%        x_width: width of the image panel(in mm)
%        y_width: height of the image panel (in mm)
%        outputFolderLocal: Local directory to save the panel
%        fileName: The name of the file (including extension). The default
%        extenion is SVG
%        xtickMinor: (default: 'off') Whether to add minor ticks to X-axis
%        ytickMinor: (default: 'off') Whether to add minor ticks to Y-axis
%        removeDashedLinesForBoxplot: (default: True) Remove the dashed
%        lines in boxplot

% Author: Ali Karimi <ali.karimi@brain.mpg.de>

%% Settings
% Defaults
if ~exist('ytickMinor','var') || isempty(ytickMinor)
    ytickMinor = 'off';
end

if ~exist('xtickMinor','var') || isempty(xtickMinor)
    xtickMinor = 'off';
end
if ~exist('removeDashedLinesForBoxplot','var') || ...
        isempty(removeDashedLinesForBoxplot)
    removeDashedLinesForBoxplot = true;
end

% set figure handle properties
num_axes = length(ax);
margin = 2;
fh = util.plot.setFigureHandle(fh, x_width, y_width, margin, num_axes);
% Remove dashed lines
if removeDashedLinesForBoxplot
    set(findobj(fh,'LineStyle','--'),'LineStyle','-');
end
% Only set axis handles when it exists. In conditional probability matrices
% it would be removed
if ~iscell(ax)
    if isvalid(ax)
        position = [2, 2, x_width, y_width];
        util.plot.setAxisHandle(ax, [], xtickMinor, ytickMinor, position);
    end
else
    % Accept multiple axes when using subplots
    for i = 1:length(ax)
        position = [(x_width*(i-1))+2+(i-1)*margin*2, 2, x_width, y_width];
        util.plot.setAxisHandle(ax{i}, [], xtickMinor, ytickMinor, position);
    end
end
%% Save
% Get the filetype to be saved
[~,~,ftypeWithDot] = fileparts(fileName);
if ~isempty(ftypeWithDot)
    ftype = strrep(ftypeWithDot,'.','-d');
else
    ftype ='dsvg';
end

% If output folder does not exist assume the filename is the fullname of
% the file
if ~exist('outputFolderLocal','var') || isempty(outputFolderLocal)
    print(fh,fileName,ftype);
    f_name_to_save = fileName;
else
    util.mkdir(outputFolderLocal);
    f_name_to_save = fullfile(outputFolderLocal,fileName);
    print(fh, f_name_to_save,ftype);
end
fprintf('File saved to: %s\n', f_name_to_save);
end

