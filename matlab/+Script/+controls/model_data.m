% load the data
util.clearAll
beta = util.load.beta_table();

%% Add averages
beta.mean_low_betas = cellfun(@mean, beta.low_Betas);
beta.mean_high_betas = cellfun(@mean, beta.high_Betas);

beta.mean_rinse_betas = mean(beta.rinse_Betas,2);
beta.mean_HL_betas = mean(beta.high_low_betas,2);

%% Linear modeling
lm = fitlm(beta, ...
    'mean_HL_betas ~ taste + session_index + animal_idx');