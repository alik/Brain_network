function [beta_tables_taste] = all_taste_betas()
% ALL_TASTE_BETAS read all three taste betas.
% Note: calls connect.read_matfiles_taste for each taste
% Taste strings, current: sour, salt, sweet
taste_str = util.str.taste_strings();

% cell of each taste beta tables
beta_tables_taste.sep = ...
    cellfun(@connect.read_matfiles_taste, taste_str, ...
    'UniformOutput', false);

% compare ROI names across taste qualities
beta_tables = cellfun(@(x) x{:,'beta_table'}, ...
    beta_tables_taste.sep,'UniformOutput', false);
beta_tables_taste.sep = cellfun(@(x,y) connect.preprocess.add_taste_animal(x,y), ...
    beta_tables_taste.sep, taste_str,'UniformOutput', false);
connect.preprocess.check_ROI_name_consistency(cat(1,beta_tables{:}));

% table version of the same thing
beta_tables_taste.sep_table = cell2table(beta_tables_taste.sep, ...
    'VariableNames', taste_str);

% concatenate taste session for combined analyses
beta_tables_taste.combined = cat(1, beta_tables_taste.sep{:});
end

