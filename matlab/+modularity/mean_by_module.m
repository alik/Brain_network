function [averages] = mean_by_module(betas)
%MEAN_BY_MODULE mean by module
[t_groups, t_ids] = findgroups(round(betas(:,2)));
assert(isequal(t_ids,[0,1,2]'));
averages = splitapply(@(x) mean(x, 'omitnan'), betas(:,1), t_groups)';
end

