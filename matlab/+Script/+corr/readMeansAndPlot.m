% red: 165, 33, 32
clear
mainDir = fullfile('/Users/karimia/code/mvgc_grangercausality/Data/RectalDistension', 'Means');
animalDirStruct = dir(mainDir);
% remove '.' and '..' dirs
animalNames= {animalDirStruct(3:end).name}';
assert(~any(contains(animalNames,'.')));
numAnimals = length(animalNames);

% Generate voiNames:
stubs = {'PbN','PAG','VMpo_VMb','Amygdala','Hypothalamus',...
    'Idfp','Idfm','Idys','vAIC','dAIC','PCC','MCC','ACC'};
RoiNames = [cellfun(@(x) ['L_',x,'_roi'],stubs,'UniformOutput',false)';...
    cellfun(@(x) ['R_',x,'_roi'],stubs,'UniformOutput',false)'];
numRois = length(RoiNames);
% Filters for the nomralization later on
[filtb,filta] = butter(2, [0.008 .25]);

allData=struct;
allRoisTogether=[];
% read the structure
for a = 1:numAnimals
   thisMat = matfile(fullfile(mainDir,animalNames{a},...
       'All_individual_mean_timecourses.mat'));
   curStat = thisMat.Statistics;
   curSessions = curStat.(animalNames{a});
   curSessNames = fieldnames(curSessions);
   
   for s = 1:length(curSessNames)
       % Get this session values
        thisSess = curSessions.(curSessNames{s});
        RoiVals = zeros(length(RoiNames),600);
        indices = [1:600;.5:0.5:300]';
        for r = 1:length(RoiNames)
             curRoi = thisSess.(RoiNames{r}).statistic;
             assert(isequal(curRoi(:,1),indices(:,1)) ||...
                 isequal(curRoi(:,1),indices(:,2)),...
                 'Index check');
             % remove nans if present
             curNan = isnan(curRoi(:,2));
             if any(curNan)
                 nanIndices = find(~curNan)';
                 % Make sure the nan part is a continuous initial segment
                assert(isequal(nanIndices,min(nanIndices):...
                    min(nanIndices)+length(find(~curNan))-1))
                curRoi = curRoi(~curNan,:);
             end
             RoiVals(r, ~curNan) = ...
                 zscore(filtfilt(filtb, filta,curRoi(:,2)));
        end
        RoiVals(:,curNan)=[];
        allRoisTogether = [allRoisTogether,RoiVals];
        allData.(animalNames{a}).(curSessNames{s}) = ...
            array2table(RoiVals','VariableNames',RoiNames);
   end
end

%% Generate the partial correlation figure
allVals = allRoisTogether';
[pCorrelation,pvalues] = partialcorr(allVals);
% Set the diagonal to zero
pCorrelation = pCorrelation-diag(diag(pCorrelation));

fh = figure('Name','Connectivity Matrix');ax=gca;
xwidth=5; ywidth=5;
pCorrelation_tril = tril(pCorrelation);
colorLim = [-0.15, 0.8];
imagesc(ax,pCorrelation_tril,colorLim);
 % Change the colormap to gray (so higher values are
partialCorr.cosmeticsMatrix(pCorrelation_tril,colorLim)
xlabel([]);ylabel([]);
xticks(1:length(RoiNames));yticks(1:length(RoiNames));
tickLabels = cellfun(@(x) strrep(x,'_','-'), RoiNames, 'uni',0);
tickLabels = cellfun(@(x) strrep(x,'-roi',''), tickLabels, 'uni',0);
tickLabelsNoHem = ...
    cellfun(@(x) strrep(x,'R-',''), tickLabels, 'uni',0);
tickLabelsNoHem = ...
    cellfun(@(x) strrep(x,'L-',''), tickLabelsNoHem, 'uni',0);
xticklabels(tickLabelsNoHem);yticklabels(tickLabelsNoHem);
ytickangle(-45);xtickangle(-45);
daspect([1,1,1])


%% Get the graph of connectivity
thisCorrForPlot = pCorrelation;
edgeLength = length(pCorrelation);
numComparisons = (edgeLength * (edgeLength-1))/2;
thisCorrForPlot(pvalues > 0.05/numComparisons)=0;
G = graph(thisCorrForPlot,tickLabels);
% Remove edges below a threshhold
threshSize = true;
if threshSize
    G = rmedge(G,find(abs(G.Edges.Weight)<0.05));
end
% Set the negative edges as red and the rest as blue
colorE = repmat([0.1804, 0.1922, 0.5647],numedges(G),1);
negEdges = find(G.Edges.Weight<0);
colorE(negEdges,:) = repmat([0.7647, 0, 0.0902],length(negEdges),1);

RoiNamesForGraphPlot = cellfun(@(x) strrep(x,'_','-'), RoiNames, 'uni',0);
fh = figure('Name','Connectivity Graph'); ax=gca;
h = plot(G,...'EdgeLabel',round(G.Edges.Weight,2), ...
    'LineWidth',15*abs(G.Edges.Weight),'EdgeColor',colorE,...
    'NodeLabel',tickLabelsNoHem);

% Draw Graph around a circle

theta = linspace(0,2*pi,27);%you can increase this if this isn't enough yet
a = 13; 
b = 19;
x=a*cos(theta);
y=b*sin(theta);
% Remove the overlapping point
x = x(1:end-1); y = y(1:end-1);

% Set positions
leftSide = [21:26,1:7];
rightSide = 8:20;
x(leftSide) = x(leftSide)+10;
x(rightSide) = x(rightSide)-10;
x([2,3]) = x([2,3])+4;
x([12,13]) =x([12,13])-4;
% Correct the hight of the lowest and the highest regions
y([21,20]) = y([21,20])-5;
y([19,22]) = y([19,22])-2.5;
y([7,8]) = y([7,8])+5;
y([6,9]) = y([6,9])+2.5;
% Set the coordinate for the regions to 
h.XData = x([21:26,1:7,20:-1:8]);
h.YData = y([21:26,1:7,20:-1:8]);

box off; axis off;
daspect([1,1,1])
xwidth = 10; ywidth = 30;

%% plot the raw traces as well
allValsT = array2table(allVals,'VariableNames',RoiNames);

% find the minimum and maximum
[minVal,Idx] = min(pCorrelation_tril(:));
[curMin.row,curMin.col] = ind2sub(size(pCorrelation_tril),Idx);
minCorrRoi= RoiNames([curMin.row,curMin.col]);

[maxVal,Idx] = max(pCorrelation_tril(:));
[curMax.row,curMax.col] = ind2sub(size(pCorrelation_tril),Idx);
MaxCorrROI = RoiNames([curMax.row,curMax.col]);

% Plot the correlation
fh = figure('Name','RawTraces'); ax=gca;
subplot(2,1,1);
plot(1:2:1000,allValsT{13000:13499,MaxCorrROI});
ylabel('Normalized regional BOLD response'); xlabel ('Time (s)');
legend(MaxCorrROI);
subplot (2,1,2);
plot(1:2:1000,allValsT{13000:13499,minCorrRoi});
ylabel('Normalized regional BOLD response'); xlabel ('Time (s)');
legend(minCorrRoi);
