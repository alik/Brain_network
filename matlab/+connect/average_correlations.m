function [cor] = average_correlations(correlations)
% AVERAGE_CORRELATIONS average a group of correlations
% Averge through a tanh transformation
cor.atanh = atanh(correlations);
cor.average = tanh(mean(cor.atanh, 3, 'omitnan'));
cor.tril = tril(cor.average, -1);
end

