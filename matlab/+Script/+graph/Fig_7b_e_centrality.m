%% Setup
util.clearAll;
outdir = fullfile(util.get_paths().figs,'graph_centrality');
util.mkdir(outdir)
c = util.plot.getColors();

%% Setup graph
% Read beta values and calculate their correlation
beta_tables_taste = util.load.all_taste_betas();
[avg_corr, ~, corr_atanh] = connect.correlations(beta_tables_taste.combined, ...
    {{'L', 'R'},{'L', 'R'}});
avg_corr = avg_corr.full;
% Create the graph
% Note: omit self loops removes the diagonal.
% Note: use only the lower triangle because of minor assymetries in the
% matrix
G_corr = graph(avg_corr.Variables, avg_corr.Properties.RowNames, ...
    'lower', 'omitselfloops');

%% Fix the corr_atanh variables
atanh_avg = table('Size', size(corr_atanh),...
    'VariableTypes', repmat("double", 1, height(corr_atanh)),...
    'VariableNames', corr_atanh.Properties.VariableNames,...
    'RowNames', corr_atanh.Properties.RowNames);
% Average the atanh correlation and set the diagonal to zero
atanh_arr = cellfun(@nanmean, corr_atanh.Variables);
atanh_arr(1:1+size(atanh_arr,1):end) = 0;
% make it symmetric
atanh_avg.Variables = (atanh_arr + atanh_arr.')/2;

%% Calculate centrality measures for the correlation graph
% use fisher Z-transformed correlations as the importance of the edge
edge_importance = atanh(G_corr.Edges.Weight);
% The inverse of the importance is the cost
edge_cost = 1 ./ edge_importance;
center_options = {{'degree', 'Importance', edge_importance},...
    {'closeness', 'Cost', edge_cost},...
    {'pagerank', 'Importance', edge_importance},...
    {'eigenvector', 'Importance', edge_importance},...
    {'betweenness', 'Cost',edge_cost}...
    };
% Loop over centerality measures
for i = 1:length(center_options)
    cur_option = center_options{i};
    G_corr.Nodes.(cur_option{1}) = centrality(G_corr,cur_option{:});
end

centrality_measures = G_corr.Nodes;
centrality_measures = [util.str.extract_hemi_ROI(centrality_measures.Name),...
    centrality_measures];

% assert the order between left and right is the same
assert(isequal(centrality_measures.region_name(centrality_measures.hemisphere == 'L'),...
    centrality_measures.region_name(centrality_measures.hemisphere == 'R')))

% Add volumes
volume = util.load.volume_ROI();
centrality_measures = join(centrality_measures, volume, 'Keys', {'Name'});

%% Get correlation between centrality measures
indices_measures = 4:8;
cent_forcorr = centrality_measures(:,indices_measures);
centrality_corr = corr(cent_forcorr.Variables);
% Plot the correlation betwee measures
fh = figure(); ax = gca;
m_names = cent_forcorr.Properties.VariableNames;
imagesc(centrality_corr); colormap(gray);
set(ax,'CLim',[0,1]);
colorbar
xticks(1:5)
yticks(1:5)
xticklabels(m_names)
yticklabels(m_names)
util.plot.cosmeticsSave...
    (fh, ax, 2.5, 2.5, outdir,...
    'correlation_matrix.svg', 'off','off');

% Write correlation values between centrality measures
cent_corr_T = array2table(round(centrality_corr,2), 'VariableNames',...
    m_names, 'RowNames', m_names);
writetable(cent_corr_T, fullfile(outdir, 'centrality_correlation.xlsx'));

%% line plot of the centrality measures

% Normalize centrality measures
cent_norm = centrality_measures;
cent_norm{:,indices_measures} = ...
    util.math.normalize_0_1(centrality_measures{:,indices_measures});

%  select left and right hemispheres
cent_hemis = cellfun(@(x) connect.preprocess.select_hemi_regions(cent_norm,[],x),...
    {'L', 'R'}, 'UniformOutput', false);
% Sort according to the sum of the left and right degrees
[~, I] = sort(cent_hemis{1}.degree + cent_hemis{2}.degree,'descend');
cent_hemis = cellfun(@(x) x(I,:), cent_hemis,'UniformOutput', false);
% Region name equality
assert(isequal(cent_hemis{1}.region_name, cent_hemis{2}.region_name));

fh = figure('Name', 'Centrality measures'); ax = gca;
hold on
X = 1:height(cent_hemis{1});
cent_L = cent_hemis{1};
cent_R = cent_hemis{2};
plot(X, cent_L{:,4}, '-', X, cent_L{:,5}, '--', X, cent_L{:,6}, ':',...
    X, cent_L{:,7}, '-.', X, cent_L{:,8}, '-x', 'Color', c.orange);
plot(X, cent_R{:,4}, '-', X, cent_R{:,5}, '--', X, cent_R{:,6}, ':',...
    X, cent_R{:,7}, '-.', X, cent_R{:,8}, '-x', 'Color', c.gray);
xticks(1:height(cent_hemis{1}))
xticklabels(util.str.rep_underscore(cellstr(cent_hemis{1}.region_name)))
legend(cent_L.Properties.VariableNames(:,indices_measures))
hold off

util.plot.cosmeticsSave...
    (fh, ax, 8, 4, outdir,...
    'Line_plot_Centrality.svg', 'off','on', false);
%% Plot the Histogram distribution of left vs. right hemisphere centrality measures

fh = figure(); ax = gca;

hold on
h1 = histogram(cent_hemis{1}.eigenvector,5,'BinWidth',0.2, ...
    'DisplayStyle','stairs','EdgeColor', c.orange);
h2 = histogram(cent_hemis{2}.degree,5,'BinWidth', 0.2, ...
    'DisplayStyle','stairs','EdgeColor', c.gray);
xlabel('Degree centrality')
ylabel('Number of regions')
util.plot.cosmeticsSave...
    (fh, ax, 1.9, 1.9, outdir,...
    'Hist_degree.svg', 'off','on', false);

fh = figure(); ax = gca;

hold on
h1 = histogram(cent_hemis{1}.betweenness,5,'BinWidth',0.2, ...
    'DisplayStyle','stairs','EdgeColor', c.orange);
h2 = histogram(cent_hemis{2}.betweenness,5,'BinWidth', 0.2, ...
    'DisplayStyle','stairs','EdgeColor', c.gray);
xlabel('Betweenness centrality')
ylabel('Number of regions')
util.plot.cosmeticsSave...
    (fh, ax, 1.9, 1.9, outdir,...
    'Hist_between.svg', 'off','on', false);

%% Correlation scatter plots for degree centrality 

% Correlation coefficients
disp(corr(cent_L.degree, cent_R.degree));
disp(corr(cent_L.degree, cent_L.volume,'rows','complete'));
disp(corr(cent_R.degree, cent_R.volume,'rows','complete'));

% Degree Left vs Right
fh = figure(); ax = gca;
scatter(cent_L.degree, cent_R.degree,20, 'k','x')
yfit = util.math.fit_linear(cent_L.degree, cent_R.degree);
hold on;
plot(cent_L.degree,yfit,'k-');
xlabel('Left hemi. degree')
ylabel('Right hemi. degree')
util.plot.cosmeticsSave...
    (fh, ax, 1.9, 1.9, outdir,...
    'Scatter_left_vs_Right.svg', 'on','on', false);


% Degree vs volume
fh = figure(); ax = gca;
hold on
gscatter(cent_norm.volume, cent_norm.degree, cent_norm.hemisphere, ...
    [c.orange; c.gray], 'x',4);
[y_fit_l, X_L] = util.math.fit_linear(cent_L.volume, cent_L.degree);
plot(X_L,y_fit_l,'Color', c.orange);
[y_fit_R, X_R] = util.math.fit_linear(cent_R.volume, cent_R.degree);
plot(X_R,y_fit_R,'Color', c.gray);
xlabel('Volume of region')
ylabel('Degree centrality')
util.plot.cosmeticsSave...
    (fh, ax, 1.9, 1.9, outdir,...
    'Scatter_leftRight_vs_vol.svg', 'on','on', false);

%% TTest for insular hemisphere comparisons
indices_insula = [4,6,10,17,21];
indices_non_insula = setdiff(1:29, indices_insula);
[~, p] = ttest(cent_L{indices_insula,indices_measures}(:), ...
    cent_R{indices_insula,indices_measures}(:));
fprintf('The p-value paired ttest left vs right insula: %f\n',p);

% Non insula
[~, p] = ttest(cent_L{indices_non_insula,indices_measures}(:), ...
    cent_R{indices_non_insula,indices_measures}(:));
fprintf('The p-value paired ttest left vs right for non-insula: %f\n',p);
% All cortical regions
[h,p] = ttest(cent_L{:,indices_measures}(:), ...
    cent_R{:,indices_measures}(:));
fprintf('The p-value paired ttest left vs right gustatory connectome: %f\n',p);

%% Plot and writeout the results
sorted_centrality = sortrows(G_corr.Nodes, 2, 'descend');
writetable(sorted_centrality, fullfile(outdir, 'centrality.xlsx'));
