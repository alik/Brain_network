function [] = error_plot(mean_T, sem_T, var, colors, ylimit)
%ERROR_PLOT Make the error barplot for the per run beta averages
arguments
    mean_T
    sem_T
    var
    colors = util.plot.getColors().modules;
    ylimit = [-0.12, 0.12]
end
e = errorbar(mean_T.(var),sem_T.(var));
for i=1:length(e)
    e(i).Color = colors(i, :); 
end
ylim(ylimit);
xlim([0.7,3.3])
xticks([1:3])
xticklabels(mean_T.Properties.RowNames);
end

