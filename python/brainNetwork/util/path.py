"""Utility module"""

def get_data_dir():
    """
    Get the main directory for data
    """
    return "/Users/karimia/data/brainnet_data/Figures_Tables/Fig_1"

def get_fig_dir():
    """
    This is where we save the figures
    """
    return "/Users/karimia/code/Brain_Network/python/figures"

def get_local_datadir():
    """
    The data dir for the csv files
    """
    return '/Users/karimia/code/Brain_Network/python/data'