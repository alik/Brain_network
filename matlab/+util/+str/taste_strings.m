function [taste] = taste_strings()
%TASTE_STRINGS Return the taste strings
taste = {'sour', 'salt', 'sweet'};
end

