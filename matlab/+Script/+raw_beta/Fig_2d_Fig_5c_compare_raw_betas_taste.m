%% Setup
util.clearAll;
cur_save_dir = fullfile(util.get_paths().figs,'raw_betas_taste');
util.mkdir(cur_save_dir)
beta_tbl = util.load.beta_table();
% Get colors
colors = util.plot.getColors();
% read the communities
module_idx = modularity.get_frequent_partition();
beta_tbl.module_index = module_idx{1,cellstr(beta_tbl.varname_str)}';

%% Average within each session
beta_tbl = connect.preprocess.avg_betas_session(beta_tbl);
[t_groups_taste, t_ids] = findgroups(beta_tbl.taste);
t_ids = cellstr(t_ids);
% compare averages between tastes
% Init variables
betas_tomean = {'high_low_betas_avg', 'rinse_Betas_avg', ...
    'low_Betas_avg', 'high_Betas_avg', 'diff_taste_rinse'};
correct_names = {'Taste', 'Rinse', 'Low', 'High', 'Diff'};
mean_by_taste = array2table(zeros(3,5),'VariableNames',betas_tomean, ...
    'RowNames',t_ids);
sem_by_taste = mean_by_taste;

for i = 1:length(betas_tomean)
    mean_by_taste{:,i} = splitapply(@(x) mean(x, 'omitnan'), ...
        beta_tbl.(betas_tomean{i}), t_groups_taste);
    sem_by_taste{:,i} = splitapply(@(x) util.math.sem(x), ...
        beta_tbl.(betas_tomean{i}), t_groups_taste);
end

%% Plot beta average error bar plots
vars = {'high_low_betas_avg', 'rinse_Betas_avg', 'diff_taste_rinse'};
% error line plots
for i=1:length(vars)
    cur_var = vars{i};
    fh = figure('Name', cur_var); ax = gca;
    taste_compare.error_plot(mean_by_taste, sem_by_taste, cur_var, ...
        [0,0,0], [-0.1, 0.1]);
    util.plot.cosmeticsSave(fh, ax, 2, 2, cur_save_dir,...
        strjoin({['Line_plot_all_moduls_',cur_var],'svg'}, '.'), ...
        'off','on', true);
end
%% vartest to compare variances between rinse and tastant

comparison_varstr = {'high_low_betas_avg', 'rinse_Betas_avg'};
by_taste_vartest = cell(3,2);
% error line plots
for i = 1:length(comparison_varstr)
    cur_var = vars{i};
    by_taste_vartest(:,i) = splitapply(@(x) {x}, ...
        beta_tbl.(comparison_varstr{i}), t_groups_taste);
end

h=zeros(3,1);
p=zeros(3,1);
for j=1:height(by_taste_vartest)
    [h(j), p(j)] = vartest2(by_taste_vartest{j,1}, by_taste_vartest{j,2});
end
%@(x) {x}

%% ANOVA test: the difference between tastes are significant for
% both rinse and tastant betas
for i = [1:2,5]
    cur_beta = betas_tomean{i};
    [p_1, tbl_1, stats_1] = anova1(beta_tbl.(betas_tomean{i}), ...
        beta_tbl.taste, 'on');
    fprintf('Difference ANOVA p-value: %d\n', p_1)
    c = multcompare(stats_1);
end

%% group by communities
by_module_mean_T = table();
by_module_std_T = table();
% Results of the ANOVA comparison of tastants within each module and
% taste/rinse condition
anova_p_taste_T = table();
anova_taste_mult_T = table();
% ANOVA results between module, fixed taste
anova_p_module_T = table();
anova_module_mult_T = table();
for i = [1,2,5]%1:length(betas_tomean)
    cur_var = betas_tomean{i};
    cur_var_name = correct_names{i};
    disp(cur_var)
    cur_average = splitapply(@(x) {modularity.mean_by_module(x)}, ...
        beta_tbl{:,{cur_var,'module_index'}}, t_groups_taste);
    cur_sem = splitapply(@(x) {modularity.split_by_module(x, @(x) util.math.sem(x))}, ...
        beta_tbl{:,{cur_var,'module_index'}}, t_groups_taste);
    % setup split for anova
    cur_split = splitapply(@(x) {modularity.split_by_module(x, @(x) {x})}, ...
        beta_tbl{:,{cur_var,'module_index'}}, t_groups_taste);
    % anova_test
    module_names = {'module_1', 'module_2','module_3'}';
    cur_split_T = cell2table(cat(1,cur_split{:}), 'VariableNames', module_names, 'RowNames',t_ids);
    % taste quality differences
    disp('Between taste comparison:')
    [cur_p_anova_taste, cur_anova_taste_T] = arrayfun(@(x) ...
        taste_compare.anova1(cur_split_T{:,x}, t_ids, cur_split_T.Properties.VariableNames{x}),...
        1:3, 'UniformOutput', false);
    % test for inter-modular differences
    disp('Between module comparison:')
    [cur_p_anova_modules, cur_anova_module_T] = arrayfun(@(x) taste_compare.anova1(cur_split_T{x, :}', ...
        module_names, cur_split_T.Properties.RowNames{x}),...
        1:3, 'UniformOutput', false);
    
    % Collect mean and sems per module and taste quality
    by_module_mean_T = [by_module_mean_T, ...
        table(cat(1,cur_average{:}), 'RowNames',t_ids,...
        'VariableNames', {cur_var_name})];
    by_module_std_T = [by_module_std_T, ...
        table(cat(1,cur_sem{:}), 'RowNames',t_ids,...
        'VariableNames', {cur_var_name})];
    
    % Collect by taste ANOVA results
    anova_p_taste_T = [anova_p_taste_T, ...
        table(cat(2, cur_p_anova_taste{:}), 'RowNames',{'p-val'},...
        'VariableNames', {cur_var_name})];
    anova_taste_mult_T = [anova_taste_mult_T, ...
        table(cat(1, cur_anova_taste_T{:}), ...
        'VariableNames', {cur_var_name})];
    % Collect by module ANOVA test results
    anova_p_module_T = [anova_p_module_T, ...
        table(cat(2, cur_p_anova_modules{:}), 'RowNames',{'p-val'},...
        'VariableNames', {cur_var_name})];
    anova_module_mult_T = [anova_module_mult_T, ...
        table(cat(1, cur_anova_module_T{:}), ...
        'VariableNames', {cur_var_name})];
end
disp(by_module_mean_T)
disp(by_module_std_T)
% Write tables of the anova and multicomparison results to excel file
writetable(splitvars(anova_taste_mult_T), ...
    fullfile(cur_save_dir, 'taste_anova_multcompare.xlsx'))
writetable(splitvars(anova_module_mult_T), ...
    fullfile(cur_save_dir, 'module_anova_multcompare.xlsx'))
%% Boxplot by community
% add rinse vs taste  vs high vs low as grouping variable
fh = figure(); ax = gca();
boxplot(beta_tbl.diff_taste_rinse, {beta_tbl.module_index,...
    beta_tbl.taste},'BoxStyle','filled','Symbol','' ,'Colors', ...
    util.plot.getColors().taste'); ylim([-2,2]);
util.plot.cosmeticsSave(fh, ax, 4, 2, cur_save_dir,...
    strjoin({'difference_betas_per_module','svg'}, '.'), 'off','off');

%% Error bar line plot
vars = {'Taste', 'Rinse', 'Diff'};
% error line plots
for i=1:length(vars)
    cur_var = vars{i};
    fh = figure(); ax = gca;
    taste_compare.error_plot(by_module_mean_T, by_module_std_T, cur_var);
    util.plot.cosmeticsSave(fh, ax, 2, 2, cur_save_dir,...
        strjoin({['Line_plot_per_module_',cur_var],'svg'}, '.'), ...
        'off','on', true);
end

%% TODOS : 1. Add the N-way anova analysis from ANOVA_combined_regions
%   N-way anova, TODO: could also add rinse vs. tastant as a variable

% [p, tbl, stats, terms] = ...
%     anovan(split_table.high_low_betas{1}, split_table{:,[1:5]},'varnames',...
%     var_names(1:5),'model', 'interaction');
% [results, ~, ~, gnames] = multcompare(stats);

