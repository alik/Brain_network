%% Setup
util.clearAll;
cur_save_dir = fullfile(util.get_paths().figs,'ANOVA_correlations');
util.mkdir(cur_save_dir)
% Get the beta tables from matfiles
beta_tables_taste = util.load.all_taste_betas().sep;
beta_tbl = util.load.beta_table();

%% First do the anova for the taste betas themselves
G = findgroups(beta_tbl.taste);
var_names ={'taste','animal_index', 'varname_str', ...
    'hemisphere', 'session_index', 'rinse_Betas', 'high_low_betas'};

split_by_taste = splitapply(@(varargin) ...
    {reshape(repelem(varargin{1}, 1, 15), [], 1), ...
    reshape(repelem(varargin{2}, 1, 15), [], 1), ...
    reshape(repelem(varargin{3}, 1, 15), [], 1), ...
    reshape(repelem(varargin{4}, 1, 15), [], 1), ...
    reshape(repelem(varargin{5}, 1, 15), [], 1), ...
    reshape(varargin{6}, [], 1), ...
    reshape(varargin{7}, [], 1)}, ...
    beta_tbl(:,var_names), G);

split_vector = arrayfun(@(x) cat(1, split_by_taste{:,x}), ...
    1:size(split_by_taste, 2), 'UniformOutput', false);
split_table = cell2table(split_vector, 'VariableNames', var_names);

%% N-way anova, TODO: could also add rinse vs. tastant as a variable
[p, tbl, stats, terms] = ...
    anovan(split_table.high_low_betas{1}, split_table{:,[1:5]},'varnames',...
    var_names(1:5),'model', 'interaction');
[results, ~, ~, gnames] = multcompare(stats);

%% oneway anova
beta_tastants_vector = cat(1, split_by_taste{:,6});
taste_groups = cat(1, split_by_taste{:,1});
[p_1,tbl_1,stats_1] = anova1(beta_tastants_vector, taste_groups);
c = multcompare(stats_1);

%% get the Z transform of the correlations for the ANOVA testing
fun_corr = @(mat_T) connect.correlations(mat_T, {{'L', 'R'}, {'L', 'R'}});
[~, ~, corr_atanh] = ...
    cellfun(fun_corr, beta_tables_taste, 'UniformOutput', false);

