function [out_T] = extract_hemi_ROI(concat)
%EXTRACT_HEMI_ROI Get the hemisphere and ROI info from the concatenated
%version of them
splits = cellfun(@(x) strsplit(x, '_'), ...
    concat,'UniformOutput',false);

hemisphere = categorical(cellfun(@(x) x{1}, splits, 'UniformOutput',false));
region_name = categorical(cellfun(@(x) strjoin(x(2:end),'_'), ...
    splits, 'UniformOutput',false));

out_T = table(hemisphere, region_name);
end

