%% Setup
util.clearAll;
outdir = fullfile(util.get_paths().figs,'chord_modularity');
util.mkdir(outdir)
c = util.plot.getColors();
%% Get the correlations
beta_tables_taste = util.load.all_taste_betas();
taste_names = beta_tables_taste.sep_table.Properties.VariableNames;
% Note: Correlations are between tastant (high_low_betas)
[~, ~, corr_atanh] = connect.correlations(beta_tables_taste.combined, ...
    {{'L', 'R'}, {'L', 'R'}});

%% First get the right indices for each module
region_order = corr_atanh.Properties.RowNames;

% indices_lower
[order_index, module_index] = connect.preprocess.sort_by_module(region_order);

% Fix the order of corr_atanh
corr_atanh_sorted = corr_atanh(order_index, order_index);

% Get indices for each valid module pair
num_mods = 3;
num_regions = length(module_index.Variables);

idx_mod = zeros(6,2);
[idx_mod(:, 1), idx_mod(:, 2)] = ind2sub([num_mods,num_mods], ...
    find((util.math.tril(zeros(num_mods,num_mods),1))'));
% start modules at 1
module_index.Variables = module_index.Variables + 1;

% pairs
indices_mod_pairs = arrayfun(@(d1, d2) [module_index.Variables' == d1, module_index.Variables' == d2], ...
    idx_mod(:, 1), idx_mod(:, 2), 'UniformOutput', false);

% colors
colors = round(c.modules * 255);
% save the colors
colors = table(colors, 'RowNames', {'1', '2', '3'}, 'VariableNames', {'RGB'});
writetable(colors, fullfile(outdir, 'colors_mod_averaged.csv'), ...
    'WriteRowNames', true)

%% Set up per module average index
avg_matrix = array2table(zeros(num_mods, num_mods), ...
    'VariableNames', {'1', '2', '3'}, ...
    'RowNames', {'1', '2', '3'});
indices_lower = util.math.indices_tril(corr_atanh);

for i = 1:length(indices_mod_pairs)
    % Get the indices for current module pair
    cur_idx = indices_mod_pairs{i};
    square_idx = zeros(num_regions, num_regions);
    square_idx(cur_idx(:,1), cur_idx(:,2)) = 1;
    cur_final_idx = (indices_lower & square_idx);

    % Get the correlations of this module pair
    cur_betas_by_taste = connect.concat_correlations(corr_atanh_sorted,...
        cur_final_idx);
    
    % Get means +- sems
    cur.means = mean(cur_betas_by_taste, 'omitnan');
    cur.sems = util.math.sem(cur_betas_by_taste);
    
    % back transform all from atanh
    cur_tanh = structfun(@tanh, cur, 'UniformOutput', false);
    
    % Set the values:
    avg_matrix{idx_mod(i, 1), idx_mod(i,2)} = cur_tanh.means;
    % make the matrix symmetric
    %avg_matrix{idx_mod(i, 2), idx_mod(i,1)} = cur_tanh.means;
end
% figure props and save
f_name = fullfile(outdir, 'mod_averaged.csv');
writetable(avg_matrix, f_name, 'WriteRowNames', true);
