function [colors] = getColors()
% GETCOLORS return structure with the most common RGB values used
% Note use: c = util.plot.getColors();

% Author: Ali Karimi <ali.karimi@brain.mpg.de>
colors.gray = [157/255,157/255,156/255];
colors.black = [0,0,0];
colors.orange = [243/255,146/255,0/255];
colors.green = [109,181,70]./255;
colors.magenta = [158,32,103]./255;
colors.cyan = [0,113,188]./255;
colors.red = [195,0,23]./255;
colors.blue = [46,49,144]./255;


colors.modules = [0.941176470588235,0.501960784313726,0.501960784313726;...
    0.250980392156863,0.878431372549020,0.815686274509804;...
    0.254901960784314,0.411764705882353,0.882352941176471];

colors.taste = [201, 145, 192; 123, 173, 213; 212, 219, 149]./255;
end

