function [session_info] = read_matfiles_taste(taste)
% READ_MATFILE_FOLDER read the sessions for a specific taste
% INPUT:
%   taste: (str) the taste string: sweet, sour, salt
% OUTPUT:
%   session_info: the table of all session matfiles
% Get the names of all the matfiles
data_path = util.get_paths().data_not_versioned;
taste_path = fullfile(data_path, 'beta_data', taste);
matfile_info = dir(fullfile(taste_path, 'session*/*_trimmed.mat'));

% create table of directory
matfile_table = struct2table(matfile_info);

% add session names as row names
session_names = ...
    cellfun(@(x) extractAfter(x, strcat(taste, filesep)),...
    matfile_table.folder,'UniformOutput', false);
matfile_table.Properties.RowNames = session_names;

% Read all matfiles
num_sessions = height(matfile_table);
matfile_content = cell2table(cell(num_sessions, 1),...
    'VariableNames', {'beta_table'}, 'RowNames', session_names);

% Loop over sessions
for i = 1:num_sessions
    cur_dir = matfile_table(i, :);
    cur_sess_str = cur_dir.Properties.RowNames{1};
    cur_matfile = load(fullfile(cur_dir.folder{1}, cur_dir.name{1}));
    cur_matfile = cur_matfile.ROI_beta_table_trimmed;
    
    % merge high and low betas
    beta_level_str = {'high_Betas', 'low_Betas'};
    %TODO: convert the high and low betas to cell arrays for merging
    cur_betas_merged = mergevars(cur_matfile, beta_level_str,...
        'NewVariableName', 'high_low_betas');
    
    % Keep the separated high and low betas
    cur_roi_betas = [cur_betas_merged, cur_matfile(:, beta_level_str)];
    
    % Add roi names as row names and delete the variable
    cur_roi_betas.Properties.RowNames = cur_roi_betas.ROIs;
    cur_roi_betas.ROIs = [];
    
    % Add hemisphere and ROI name
    cur_hemi_name = connect.get_roi_info(cur_roi_betas.Properties.RowNames);
    cur_roi_betas = join(cur_hemi_name, cur_roi_betas, 'Keys', 'Row');
    
    % Convert all betas to a numeric array from cell
    cur_roi_betas.all_Betas = cat(2, cur_roi_betas.all_Betas{:})';
    
    % Add the rinse_betas
    indices_rinse = 31:2:59;
    cur_roi_betas.rinse_Betas = cur_roi_betas.all_Betas(:, indices_rinse);
    
    % sort the rows according to hemisphere and name
    cur_roi_betas = sortrows(cur_roi_betas, {'hemisphere', 'region_name'});
    
    % convert to cell array
    for j = 1:length(beta_level_str)
        cur_level = beta_level_str{j};
        cur_roi_betas.(cur_level) = num2cell(cur_roi_betas.(cur_level), 2);
    end
    
    % Add to matfile content
    matfile_content{cur_sess_str, 'beta_table'}{1} = cur_roi_betas;
end

% Merge trees
session_info = connect.get_session_info(session_names);
session_info = join(session_info, matfile_content, 'Keys', 'Row');

% Fix missing amygdala data in salt sessions
session_info = connect.preprocess.fix_sessions(session_info);

% remove duplicated regions
session_info.beta_table = cellfun(@connect.preprocess.remove_dup_ROIs, ...
    session_info.beta_table, 'UniformOutput', false);

% sort rows based on the hemisphere and region name
session_info = sortrows(session_info, {'session_index'});
end

