import os
from nilearn import plotting
from brainNetwork.util.path import get_data_dir

# Load example surface from the left hemisphere
f_name = "lh.gray_surface.rsl.gii"
example_surface = os.path.join(get_data_dir(), "template_surfaces", f_name)
view = plotting.plot_surf_contours(surf_mesh = example_surface,
                          title = f_name,
                          black_bg = True)

view.open_in_browser()