%% Script to compare rinse and tastant trial beta values 
% Rinse betas indices all_beatas 31:2:59
util.clearAll;
outdir = fullfile(util.get_paths().figs,'controls', 'rinse');
util.mkdir(outdir)

load(fullfile(util.get_paths().data_versioned, 'betas', 'full_beta_tbl.mat'))
% Find taste groups
[G, TID] = findgroups(final_beta_table(:, 'taste'));

%% Average over the sessions
% Get per session average of betas for rinse, low- and high-concentration
% betas
rinse_b = mean(final_beta_table.rinse_Betas, 2);
high_vs_low_b = cellfun(@nanmean,final_beta_table{:,{'low_Betas','high_Betas'}});
betas_rinse_high_low = [rinse_b, high_vs_low_b];
% Split by taste as well
betas_split = splitapply(@(x) {x}, betas_rinse_high_low, G);
betas_split = betas_split';
betas_split = cellfun(@(x) num2cell(x,1), betas_split, ...
    'UniformOutput', false);
betas_split = cat(2,betas_split{:});
% create a 9 x num sessions array of average beta values for the rinse,
% tastants for each taste quality
betas_split_arr = util.padcat(betas_split{:});

%% Box plot for the average session (15 presentations) betas of rinse, low- 
% and high-concentration tastant
x_labels = ...
    Script.controls.label_names(cellstr(TID{:,:}), {'rinse', 'low', 'high'});

c = util.plot.getColors();
cur_c = [c.gray; c.magenta; c.red];
cur_c = repmat(cur_c, 3, 1);

% Note: outliers are not displayed with 'symbol' being empty char
fh = figure(); ax = gca;
boxplot(betas_split_arr, 'Colors', cur_c, 'Jitter', 1,'symbol', '',...
    'PlotStyle', 'traditional')
set(ax, 'XTick', 1:9, 'XTickLabel', x_labels)
ylim([-2,2])
xlabel('')
ylabel('Average session beta')
util.plot.set_fig_size(fh, [5, 5]);
%axis([0,5.3,0, 4.1])
util.plot.set_font(ax);
util.plot.box_ticks(ax);
util.plot.cosmeticsSave...
    (fh, ax, 4., 4., outdir,...
    'Boxplot_average_session_betas.svg', 'off','on', true);


%% Get the rinse, high and low betas separated by taste

concat_rinse_HL = ...
    splitapply(@(x,y,z) {x(:),y(:),z(:)}, ...
    final_beta_table(:, {'rinse_Betas','low_Betas', 'high_Betas'}), G);
concat_rinse_HL(:,2:3) = cellfun(@(x) cat(2, x{:})', ...
    concat_rinse_HL(:,2:3), 'UniformOutput', false);

concat_rinse_HL = concat_rinse_HL';
concat_rinse_HL = util.padcat(concat_rinse_HL{:});

%% Violin plot
x_labels = ...
    Script.controls.label_names(cellstr(TID{:,:}), {'rinse', 'tastant'});



% Note: outliers are not displayed with 'symbol' being empty char
fh = figure(); ax = gca;
util.plot.violinplot(concat_rinse_HL, x_labels, ...
    'ShowData', false, 'ViolinColor', cur_c, 'ViolinAlpha', ...
    repmat([0.3, 0.8], 1, 3));
ylim([-3,3])
xlabel('Tastant level')
ylabel('Beta values')

%% histogram
fh = figure(); ax = gca;
hold on
for i = 1:6
    histogram(concat_rinse_HL(:, i),'Normalization','probability',...
        'DisplayStyle','stairs','EdgeColor', cur_c(i,:))
end
xlim([-3, 3])
ylabel('Fraction')
xlabel('Beta values')

%% Density plot
fh = figure(); ax = gca;

hold on
for i = 1:6
    [f_ks, xi_ks] = ksdensity(concat_rinse_HL(:, i), 'NumPoints', 2000);
    if mod(i, 2) == 0
        line_style = '-';
    else
        line_style = '--';
    end
    plot(xi_ks, f_ks, 'Color', cur_c(i, :), 'LineStyle', line_style);
end
legend(x_labels)
xlim([-3, 3])
ylabel('Probability density estimate')
xlabel('Beta values')
util.plot.set_fig_size(fh, [10, 10]);
util.plot.set_font(ax);
util.plot.box_ticks(ax);
exportgraphics(ax, ...
    fullfile(outdir, 'Density_rinse_tastant.pdf'), ...
    'ContentType', 'vector')