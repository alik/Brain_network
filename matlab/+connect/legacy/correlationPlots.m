function [maxFig minFig] = correlationPlots(condTimeseries, pCorrelation_tril, roiLabels)
% This function calculates representative correlation and anticorrelation
% traces from the partial correlation analysis
%
% Compiler(s): Renee Hartig (renee.hartig@tuebingen.mpg.de) & Ali Karimi
% (ali.karimi@brain.mpg.de)
%
% Inputs:
% condTimeseries % concatenated regional BOLD correlations
% pCorrelation % array of correlation values
% roiLabels % string of regions-of-interest (ROIs)
%
% Outputs:
% maxFig % plots of the maximally correlated regions
% minFig % plots of the minimally correlated regions

%% Plot the raw traces as well

% find the minimum and maximum
for i = 1:length(pCorrelation_tril)
    % identify the minimum
    [minVal,Idx] = min(pCorrelation_tril{i}(:));
    [curMin.row,curMin.col] = ind2sub(size(pCorrelation_tril{i}),Idx);
    minCorrROI{i} = roiLabels([curMin.row,curMin.col]);
    % check that minimum is not a region with itself
    if isempty(setdiff(minCorrROI{i}{1}, minCorrROI{i}{2})) == 1
        % set that value to 0.3
        pCorrelation_tril{i}(Idx) = 0.3;
        % identify the next minimum
        [minVal,Idx] = min(pCorrelation_tril{i}(:));
        [curMin.row,curMin.col] = ind2sub(size(pCorrelation_tril{i}),Idx);
        minCorrROI{i} = roiLabels([curMin.row,curMin.col]);
    end
    % identify the maximum
    [maxVal,Idx] = max(pCorrelation_tril{i}(:));
    [curMax.row,curMax.col] = ind2sub(size(pCorrelation_tril{i}),Idx);
    maxCorrROI{i} = roiLabels([curMax.row,curMax.col]);
    % check that maximum is not a region with itself
    if isempty(setdiff(maxCorrROI{i}{1}, maxCorrROI{i}{2})) == 1
        % set that value to 0.3
        pCorrelation_tril{i}(Idx) = 0.3;
        % identify the next maximum
        [maxVal,Idx] = max(pCorrelation_tril{i}(:));
        [curMax.row,curMax.col] = ind2sub(size(pCorrelation_tril{i}),Idx);
        maxCorrROI{i} = roiLabels([curMax.row,curMax.col]);
    end
end

%% Plot the correlation
% turn timeseries array into table with ROI labels
% take snipit of frames (10:144)
condTimeseries = array2table(condTimeseries(10:144,:), 'VariableNames',roiLabels);

for i = 1:length(maxCorrROI)
% create figure for maximum
maxFig(i) = figure('Name',' MaximumCorrelation'); 
% plot data at the for specific regions (vars)
varsMax = {{maxCorrROI{i}{1},maxCorrROI{i}{2}}}; 
% use stackedplot function for plotting data from tables
stackedplot(condTimeseries, varsMax, 'Title', 'Correlated Region Activity', ...
     'DisplayLabels', {'Correlation Coefficient'});

 % create figure for minimum
minFig(i) = figure('Name','MinimumCorrelation');
% plot  at the for specific regions (vars)
varsMin = {{minCorrROI{i}{1},minCorrROI{i}{2}}}; 
% use stackedplot function for plotting data from tables
stackedplot(condTimeseries, varsMin, 'Title', 'Anticorrelated Regional Activity', ...
     'DisplayLabels', {'Correlation Coefficient'});
end
end