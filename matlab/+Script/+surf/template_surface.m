%% read cortex surface from gifti
gifti_name = dir(fullfile(util.get_data_dir, ...
    'template_surfaces/*.gii'));
template_surf = cell(size(gifti_name));
for i = 1:length(template_surf)
    cur_name = gifti_name(i);
    template_surf{i} = util.get_surface_from_gifti...
        (fullfile(cur_name.folder, cur_name.name));
end
template_surf_T = cell2table(cell(size(gifti_name)), ...
    'VariableNames', {'surface'},...
    'RowNames',  {gifti_name.name}');
template_surf_T.Variables = template_surf;

color = [.3, .3, .3];
for i = 1:height(template_surf_T)
    cur_name = template_surf_T.Properties.RowNames{i};
    fh = figure('Name', cur_name);
    util.display_surface(template_surf_T{i, 1}{1}, color);
    camproj('perspective');
    camlight;
    view([135, 25])
    lighting gouraud
end

% low-res rh.gray_surface.inf_300.rsl.gii , lh.gray_surface.inf_300.rsl.gii 
% high-res lh.gray_surface.rsl.gii, rh.gray_surface.rsl.gii