import os

import pandas as pd
import numpy as np
from nilearn import plotting

from brainNetwork.util.path import get_data_dir, get_fig_dir

template_name = os.path.join(get_data_dir(),"base_template_image/NMT_v2.0_sym_SS.nii")
roi_name = os.path.join(get_data_dir(),"connectome_nodes/cortical/composite_image/CHARM_mask.nii.gz")

plotting.plot_roi(roi_img=roi_name,
                  bg_img=template_name,
                  output_file=os.path.join(get_fig_dir(),"roi_image.svg"))
plotting.show()