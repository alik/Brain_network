function [X, X_tril_vec] = tril(X, val)
%TRIL Create a lower triangle matrix by setting the upper part to val
% INPUT:
%   X: m x n matrix
%   val: value to set the upper triangle to
% OUTPUT:
%   X: m x n lower triangular matrix
%   X_tril_vec: vector of the lower triangular elements
arguments
    X 
    val = nan
end
indices_lower = util.math.indices_tril(X);
X(~indices_lower) = val;
X_tril_vec = X(indices_lower);
end

