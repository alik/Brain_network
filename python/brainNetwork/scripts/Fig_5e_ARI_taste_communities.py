import os
from pathlib import Path

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

import networkx as nx
from brainNetwork.util.path import get_local_datadir, get_fig_dir
from brainNetwork.util import connectome, util

# Paths
csv_dir = Path(get_local_datadir(), 'connectivity_matrices',
               'connectivity_tastant_combined.csv')
save_dir = Path(get_local_datadir(), 'shuffled_modularity', 'ARI')
save_dir.mkdir(exist_ok=True)

# Read the matrix
connect_mat = pd.read_csv(csv_dir, index_col=0)

# Get the graph and edge list
G = nx.convert_matrix.from_pandas_adjacency(connect_mat)
df_pre_post = nx.to_pandas_edgelist(G)

# calculate and save permutated modularities
get_shuffling = False
df_shuf_partitions, df_shuf_modularities = connectome.permute_modularity(pre_post_list=df_pre_post,
                                                                         output_path=save_dir,
                                                                         num_permute=10000,
                                                                         perform_repetition=get_shuffling)

# Calculate ARI for random shuffles
shuf_ARI = connectome.rand_shuffled(df_shuf_partitions=df_shuf_partitions,
                                    output_path=save_dir,
                                    perform_calculation=False,
                                    num_samples=10000)

# Get the partitions from each tastant and combined
sol_str = ['tastant', 'rinse']
taste_str = ['salt', 'sour', 'sweet']
df_obs_partitions = connectome.get_per_taste_folder(
    f_name='unique_partitions.csv', sol_str=sol_str, taste_str=taste_str)

# sort by region name for consistency
df_obs_partitions['all_partitions'] = df_obs_partitions['paths'].apply(
    lambda x: pd.read_csv(x, index_col=0).sort_index())
df_obs_partitions['partitions'] = df_obs_partitions['all_partitions'].apply(
    lambda x: pd.DataFrame(x.iloc[:,-1]))

df_agg_by_sol = df_obs_partitions.groupby(['solution']).agg(
    {'partitions': lambda x: [pd.concat(x.values, axis=1)]})
df_ARI_within_sol = df_agg_by_sol['partitions'].map(
    lambda x: connectome.measure_rand_index(x[0], tril=True))

df_ARI_within_between = connectome.measure_rand_df_pair(df_agg_by_sol['partitions'][0][0], 
                                                        df_agg_by_sol['partitions'][1][0])
# Plot the histogram
fig, ax = plt.subplots(figsize=util.cm2inch(3.3, 1.8))
vars = ['tastant', 'rinse']
colors = ['m', 'b', [0.5, 0.5, 0.5]]
ypos = [0.10, 0.05, 0.15]
# Plot scatter points for tastant and rinse separately
for i, var in enumerate(vars):
    cur_ARI = df_ARI_within_sol.loc[var]['ARI']
    plt.scatter(x=cur_ARI, y=np.repeat(ypos[i], len(cur_ARI)), 
                    color=colors[i], s=4, marker='x',linewidths=0.5)
# Comparison between tastant and rinse
plt.scatter(x=df_ARI_within_between['ARI'], y=np.repeat(ypos[2], len(df_ARI_within_between['ARI'],)), 
                color=colors[2], s=4, marker='x',linewidths=0.5)

# Plot random histogram
sns.histplot(shuf_ARI['ARI_shuffled'],
             stat='proportion',
             binwidth=0.01,
             color='k',
             element='step',
             fill=False,
             binrange=[-0.05, 1])
plt.xlim([-0.1, 1])
plt.xticks(np.arange(0, 1.05, 0.2))
plt.yticks(np.arange(0, 0.21, 0.05))
util.remove_box(plt.gca())
plt.savefig(save_dir.joinpath('shuffled_ARI_histogram.svg'))
pass
