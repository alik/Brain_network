function [volume] = sub_volume(volume, hemisphere)
%SUB_VOLUME select the hemispheres to keep in the volume
lateral_size = size(volume, 2);
assert(rem(lateral_size, 2) == 0);
center_line = lateral_size / 2;
switch hemisphere
    case 'right'
        volume(:, 1: center_line, :) = 0;
    case 'left'
        volume(:, center_line + 1: center_line * 2, :) = 0;
    case 'both'
        disp('Nothing done on for both slices')
    otherwise
        error('Incorrect switch case')
end
end

