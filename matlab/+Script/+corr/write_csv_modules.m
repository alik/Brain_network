%% Setup
% This script writes CSV files for the chord diagram in R
% Note: the tastes are combined now
util.clearAll;
% stimuli 
cur_save_dir = fullfile(util.get_paths().figs,'CSV_modules');
util.mkdir(cur_save_dir)

%% Write the CSV files for chord diagram (R)
regions = {'PFC', 'OFC', 'ACC_area24', 'agranularIns_bin', ...
    'dAIC_custom_bin_agranTrim', 'Idys','Idfm_custom_bin', 'S2', 'S1',...
    'thalamus_ohne_genicula', 'hypothalamus', 'amygdala', 'putamen', ...
    'PAG', 'PBC'};

% Get the beta tables from matfiles
taste_str = util.str.taste_strings();
beta_tables_taste = ...
    cellfun(@connect.read_matfiles_taste, taste_str, ...
    'UniformOutput', false);

% select specific ROIs
hemi = 'L';
for i = 1:length(beta_tables_taste)
    beta_tables_taste{i}.beta_table = ...
        cellfun(@(x) connect.preprocess.select_regions(x, regions, hemi),...
        beta_tables_taste{i}.beta_table, 'UniformOutput', false);
    
end
% Get the correlations
combined_beta = cat(1, beta_tables_taste{:});
avg_correlations = connect.correlations(combined_beta, {hemi, hemi}, 'region_name');

%% write the CSV files
f_name = fullfile(cur_save_dir, [hemi,'_all_tastes.csv']);
writetable(avg_correlations.full, f_name, 'WriteRowNames', true);

%% colors
region_info = util.get_region_info();
color_regions = {'PFC', 'OFC', 'ACC_area24', 'vAIC', 'dAIC', 'Idys', ...
    'Idfm', 'S2', 'S1', 'Thalamus', 'Hypothalamus', 'Amygdala', 'Putamen',...
    'PAG', 'PBC'};
region_colors = region_info(color_regions, 'RGB');
region_colors.Properties.RowNames = regions;
writetable(region_colors, fullfile(cur_save_dir, 'colors.csv'), ...
    'WriteRowNames', true)