%% clear all
close all;
clear;
outdir = fullfile(util.get_paths().figs, 'connectivity');
util.mkdir(outdir);

%% Load matfile data
% Taste and hemisphere combinations
taste = {'sour', 'salt', 'sweet'};
hemi_combos = {{'R', 'R'}};
matrix_type = {'full'};
num_taste = length(taste);
num_hemi_combos = length(hemi_combos);
ttest_results = cell(num_taste, num_hemi_combos);
for taste_idx = 1:num_taste
    % load the matfile contents into a table
    cur_taste = taste{taste_idx};
    cur_matfile_T = connect.read_matfiles_taste(cur_taste);

    for hemi_idx = 1:num_hemi_combos
        cur_hemi = hemi_combos{hemi_idx};
        cur_mat_type = matrix_type{hemi_idx};
        cur_name = strjoin({'correlation', cur_taste, strjoin(cur_hemi, '_')}, '_');
        cur_name_dash = util.str.rep_underscore(cur_name);
        
        fprintf('Experiment name: %s\n', cur_name);
        % Correlations: get the average correlations
        [avg_corr, correlations] = ...
            connect.correlations(cur_matfile_T, cur_hemi);
        
        % Min and Max correlations
        disp(['Min and Max correlations: ', ...
            num2str(util.math.min_max(avg_corr.full.Variables))]);
        
        % ttest for significant correlations
        [ttest_results{taste_idx, hemi_idx}, ttest_p] = ...
            connect.ttest(correlations);
        
        % correlogram
        fh = figure('Name',['Connectivity Matrix: ',...
            cur_name_dash],...
            'Position', [10 10 800 800]);
        ax = gca;
        title(cur_name_dash)
        cur_save_name = fullfile(outdir, strjoin({cur_name,'pdf'}, '.'));
        connect.plot_matrix(...
            avg_corr.(cur_mat_type), cur_mat_type);
        util.plot.box_ticks(ax)
        print(cur_save_name, '-dpdf');
    end
end