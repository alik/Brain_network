function [region_info] = get_region_info()
%GET_REGION_INFO return region info
m = load(fullfile(util.get_paths().data_versioned,'region_info',...
    'initial.mat'));
region_info = m.initial;
region_info.RGB_normalized = region_info.RGB ./ 255;
region_info.Properties.RowNames = cellstr(region_info.Name);

% Merge, X, Y, Z into coordinate
coord_var_names = {'X', 'Y', 'Z'};
region_info = mergevars(region_info, coord_var_names, ...
    'NewVariableName','coords');
end

