function [matfile_table] = fix_sessions(matfile_table)
% FIX_SINGLE_SESSION fix sessions where amygdala is missing. In addition,
% check consistency of ROI names amongst sessions with or without amygdala
% INPUT:
%   matfile_table: table of session beta values
% Get number of ROIs in each session
num_sess_ROIs = cellfun(@height, matfile_table.beta_table);
num_ROIs_unique = sort(unique(num_sess_ROIs));
% Decide depending on whether all a
switch length(num_ROIs_unique)
    case 1
        disp('No need for correction');
        % Check name consistency for all the sessions
        connect.preprocess.check_ROI_name_consistency...
            (matfile_table.beta_table);
        return;
    case 2
        assert(isequal(num_ROIs_unique, [58;60]))
    otherwise
        disp('Not recognized number of different ROIs')
end

% Indices for the current number of ROI sessions
session_idx = arrayfun(@(x) find(num_sess_ROIs == x), ...
    num_ROIs_unique, 'UniformOutput', false);

% Check consistency of ROI names within sessions with and without amygdala
cellfun(@(x) ...
    connect.preprocess.check_ROI_name_consistency(matfile_table.beta_table,x), ...
    session_idx, 'UniformOutput', false);

% regions with missing amygdala
sess_idx_wo_amygdala = session_idx{1};
% Get example session with amygdala for setting up the corrected tables
w_amy_idx = session_idx{2}(1);
example_sess_w_amy = matfile_table.beta_table{w_amy_idx};

for i = 1:length(sess_idx_wo_amygdala)
    cur_index = sess_idx_wo_amygdala(i);
    cur_session_wo_amy = matfile_table.beta_table{cur_index};
    % Find the index of missing ROIs in session with AMYGDALA
    [~, Locb] = ismember(cur_session_wo_amy.Properties.RowNames, ...
        example_sess_w_amy.Properties.RowNames);
    new_wo_session = util.create_empty_table(example_sess_w_amy);
    % Fill betas with nan first
    
    new_wo_session.high_low_betas = nan(size(example_sess_w_amy.high_low_betas));
    new_wo_session.all_Betas = nan(size(example_sess_w_amy.all_Betas));
    new_wo_session.rinse_Betas = nan(size(example_sess_w_amy.rinse_Betas));
    
    new_wo_session.high_Betas = cellfun(@(x) nan(size(x)), ...
        example_sess_w_amy.high_Betas, 'UniformOutput', false);
    new_wo_session.low_Betas = cellfun(@(x) nan(size(x)), ...
        example_sess_w_amy.low_Betas, 'UniformOutput', false);
    % basic properties should not change
    basic_props = {'hemisphere','region_name',...
        'identifier_str','varname_str'};
    new_wo_session{:, basic_props} = example_sess_w_amy{:, basic_props};
    % set values where available
    beta_var_names = {'all_Betas', 'high_low_betas', ...
        'high_Betas', 'low_Betas', 'rinse_Betas'};
    new_wo_session(Locb, beta_var_names) = ...
        cur_session_wo_amy(:, beta_var_names);
    matfile_table.beta_table{cur_index} = new_wo_session;
end
% check consistency again
connect.preprocess.check_ROI_name_consistency...
    (matfile_table.beta_table);
end
