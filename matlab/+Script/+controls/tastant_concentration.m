%% Setup
% This script is for checking the controls
% TODO: First step: keep the high and low betas separate and get the rinse

util.clearAll;
outdir = fullfile(util.get_paths().figs,'controls');
util.mkdir(outdir)

%% load the data and combine betas

% Read beta values and calculate their correlation
% TODO: First step: keep the high and low betas separate and get the rinse
beta_tables_taste = util.load.all_taste_betas();

%% Boxplot high vs low betas, sessions combined
% get a separation of high and low betas based on the columns of a cell
% array
concen.combined = util.control.get_conc_array(beta_tables_taste.combined);
concen.sep = {};
for i = 1:3
   concen.sep{i} = util.control.get_conc_array(beta_tables_taste.sep{i});
end
concen.sep = cat(2, concen.sep{:});
concen.sep = util.padcat(concen.sep{:});

%% plotting
c = util.plot.getColors();
cur_c = repmat([c.red; c.blue], 3, 1);

% Note: outliers are not displayed with 'symbol' being empty char
fh = figure(); ax = gca;
boxplot(concen.sep, 'Colors', cur_c, 'Jitter', 1,'symbol', '',...
    'PlotStyle', 'traditional')
ylim([-3, 3])
util.plot.box_ticks(ax)
taste_str = util.str.taste_strings();
conc_str = {'High', 'Low'};
[T, C] = ndgrid(taste_str, conc_str);
T = T'; T = T(:);
C = C'; C = C(:);
x_labels = cellfun(@(t,c)strcat(t,'-',c),T,C ,'UniformOutput',false);
set(ax, 'XTick', 1:6, 'XTickLabel', x_labels)
xlabel('Tastant level')
ylabel('Beta values')
exportgraphics(ax, ...
    fullfile(outdir, 'boxplot_high_low_per_taste.pdf'), ...
    'ContentType', 'vector')

%% Look at the correlations of the high betas vs low_betas as well
hemis = {{'L', 'R'}, {'L', 'R'}};
% TODO: fix the connect.correlations to accept the type of the high_beta vs
% low_beta correlations
[~, correlations, corr_atanh] = cellfun(@(x) connect.correlations(x, hemis),...
    beta_tables_taste.sep, 'UniformOutput', false);
% array of all correlations (the lower triangle part)
[~, corr_tril_vec] = cellfun(@util.math.tril,...
    correlations, 'UniformOutput', false);