function [] = matrix_labels(labels_table)
% MATRIX_LABELS add labels

% number of regions
xticks(1:width(labels_table));
yticks(1:height(labels_table));
x_labels = util.str.rep_underscore(labels_table.Properties.VariableNames);
y_labels = util.str.rep_underscore(labels_table.Properties.RowNames);
xticklabels(x_labels);
yticklabels(y_labels);
ytickangle(-45);
xtickangle(-45);

% Save image
daspect([1,1,1]);
end

