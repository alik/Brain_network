%% Setup
util.clearAll;
cur_save_dir = fullfile(util.get_paths().figs,'ANOVA_correlations');
util.mkdir(cur_save_dir)
c = util.plot.getColors();

%% Anova analysis for the pairwise region correlations
beta_tables_taste = util.load.all_taste_betas();
taste_names = beta_tables_taste.sep_table.Properties.VariableNames;

% Note: Correlations are between tastant (high_low_betas)
fun_corr = @(mat_T) connect.correlations(mat_T, {{'L', 'R'}, {'L', 'R'}});
[~, ~, corr_atanh] = ...
    cellfun(fun_corr, beta_tables_taste.sep, 'UniformOutput', false);

indices_lower = util.math.indices_tril(corr_atanh{1});
all_corr = cellfun(@(x) connect.concat_correlations(x, indices_lower), ...
    corr_atanh, 'UniformOutput', false);
g_ids = cellfun(@(taste_names, beta_array) repmat(categorical({taste_names}), ...
    length(beta_array), 1), taste_names, all_corr,'UniformOutput', false);

%% Combine correlations for ANOVA test
all_corr_comb = cat(1, all_corr{:});
g_ids_comb = cat(1, g_ids{:});
[p_anova_corr,tbl,stats] = anova1(all_corr_comb, g_ids_comb);
mult_comp = multcompare(stats);
fprintf('Tha p-value anova for taste correlation: %d\n',p_anova_corr)

%% Setup for Line plot of average +-sem
means_corr = cellfun(@(x) mean(x, 'omitnan'), all_corr);
sems_corr = cellfun(@util.math.sem, all_corr);
cur_upper = tanh(means_corr + sems_corr);
cur_lower = tanh(means_corr - sems_corr);
cur_means = tanh(means_corr);
% Print number of correlations for each tastant
total_num_corrs = array2table(cellfun(@(x)length(x(~isnan(x))),all_corr),...
    'VariableNames', taste_names);
disp(total_num_corrs);

%% Line plot of average +-sem
fh = figure(); ax = gca();
errorbar(1:length(cur_means), cur_means, ...
     cur_means-cur_lower, cur_upper-cur_means, ...
    'Color', 'k');
ylim([0.52, 0.56]);
xlim([0.7,3.3])
xticks([1:3])
xticklabels(taste_names);

util.plot.cosmeticsSave(fh, ax, 4, 4, cur_save_dir,...
    strjoin({'Line_plot_correlations','svg'}, '.'), ...
    'off','on', true);

%% Setup: Separate based on the modules

region_order = taste_compare.ROI_name_consistency(corr_atanh);

% indices_lower
[order_index, module_index] = connect.preprocess.sort_by_module(region_order);

% Fix the order of corr_atanh
corr_atanh_sorted = cellfun(@(x) x(order_index, order_index),...
    corr_atanh, 'UniformOutput', false);

% Get indices for each valid module pair
num_mods = 3;
num_regions = length(module_index.Variables);

idx_mod = zeros(6,2);
[idx_mod(:, 1), idx_mod(:, 2)] = ind2sub([num_mods,num_mods], ...
    find((util.math.tril(zeros(num_mods,num_mods),1))'));
% start modules at 1
module_index.Variables = module_index.Variables + 1;

% pairs
indices_mod_pairs = arrayfun(@(d1, d2) [module_index.Variables' == d1, module_index.Variables' == d2], ...
    idx_mod(:, 1), idx_mod(:, 2), 'UniformOutput', false);

% colors
colors = [c.modules(1,:); c.gray; c.gray*0.5; c.modules(2,:); ...
    c.black; c.modules(3,:)];
%% Go through module pairs for anova test and error line plot
anova_results = cell(num_mods, num_mods);
color_img = zeros(num_regions, num_regions);
fh = figure(); ax = gca();
hold on
for i = 1:length(indices_mod_pairs)
    % Get the indices for current module pair
    cur_idx = indices_mod_pairs{i};
    square_idx = zeros(num_regions, num_regions);
    square_idx(cur_idx(:,1), cur_idx(:,2)) = 1;
    cur_final_idx = (indices_lower & square_idx);
    % collect image
    color_img(cur_final_idx) = i;
    % Get the correlations of this module pair
    cur_betas_by_taste = ...
        cellfun(@(x) connect.concat_correlations(x, cur_final_idx), ...
        corr_atanh_sorted, 'UniformOutput', false);
    
    % run ANOVA test
    anova_results{idx_mod(i, 1), idx_mod(i, 2)} = taste_compare.anova1(...
        cur_betas_by_taste, taste_names);
    % Get means +- sems
    cur.means = cellfun(@(x) mean(x, 'omitnan'), cur_betas_by_taste);
    cur_sems = cellfun(@(x) util.math.sem(x), cur_betas_by_taste);
    cur.upper = cur.means + cur_sems;
    cur.lower = cur.means - cur_sems;
    
    % back transform all from atanh
    cur_tanh = structfun(@tanh, cur, 'UniformOutput', false);
    
    % Plot
    errorbar(1:length(cur_tanh.means), cur_tanh.means, ...
        cur_tanh.means-cur_tanh.lower, cur_tanh.upper-cur_tanh.means, ...
        'Color', colors(i, :));
end
% figure props and save
ylim([0.4, 0.7]);
xlim([0.7,3.3])
xticks([1:3])
xticklabels(taste_names);
util.plot.cosmeticsSave(fh, ax, 2, 2, cur_save_dir,...
    strjoin({'Line_plot_correlations_mod','svg'}, '.'), ...
    'off','on', true);

%% plot thumbnail for colors
fh = figure(); ax = gca();
imagesc(color_img)
colormap([[1,1,1];colors]);
axis off
daspect([1 1 1])
util.plot.cosmeticsSave(fh, ax, 2, 2, cur_save_dir,...
    strjoin({'thumbnail_colors_modules','svg'}, '.'), ...
    'off','off', true);