# Let us use a Nifti file that is shipped with nilearn
from nilearn.datasets import MNI152_FILE_PATH
from nilearn import plotting
from nilearn import image

# Note that the variable MNI152_FILE_PATH is just a path to a Nifti file
print('Path to MNI152 template: %r' % MNI152_FILE_PATH)
plotting.plot_img(MNI152_FILE_PATH)
# smooth
smooth_anat_img = image.smooth_img(MNI152_FILE_PATH, fwhm=3)
plotting.plot_img(smooth_anat_img)
# smooth even further
more_smooth_anat_img = image.smooth_img(smooth_anat_img, fwhm=3)
plotting.plot_img(more_smooth_anat_img)
# write to a file
more_smooth_anat_img.to_filename('more_smooth_anat_img.nii.gz')

plotting.show()