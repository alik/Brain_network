function [p] = display_surface(iso_surface, color, alpha)
%DISPLAY_SURFACE display a surface and set color
if ~exist('alpha','var') || isempty(alpha)
    alpha = 1;
end
p = patch(iso_surface);
p.FaceColor = color;
p.FaceAlpha = alpha;
p.EdgeColor = 'none';
daspect([1 1 1]);
axis tight;
% set limits
bbox = util.get_param().bbox_shifted;
xlim(bbox(1,:));
ylim(bbox(2,:));
zlim(bbox(3, :));
end
