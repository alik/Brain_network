import numpy as np

def cm2inch(*tupl):
    """
    Convert centimeter tuple to inch tuple for setting figure size in matplotlib
    """
    inch = 2.54
    if isinstance(tupl[0], tuple):
        return tuple(i/inch for i in tupl[0])
    else:
        return tuple(i/inch for i in tupl)


def remove_box(ax, type = 'vertical'):
    """
    Remove box from axes
    """
    if type == 'vertical':
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
    elif type == 'horizontal':
        ax.spines['bottom'].set_visible(False)
        ax.spines['right'].set_visible(False)
    return ax


def colors():
    modules = np.asarray([[0.941176470588235, 0.501960784313726, 0.501960784313726],
                          [0.250980392156863, 0.878431372549020, 0.815686274509804],
                          [0.254901960784314, 0.411764705882353, 0.882352941176471]])
    return modules

def colors_taste():
    taste_colors = np.asarray([[123, 173, 213],
                        [201, 145, 192],
                        [212, 219, 149]]) / 255
    return taste_colors

def get_tril_bool(cur_shape):
    """
    Get a lower triangle boolean array
    """
    return np.tril(np.ones(cur_shape),k=-1).astype(np.bool)
