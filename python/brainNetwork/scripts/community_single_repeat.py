from pathlib import Path

import pandas as pd
import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt

import networkx as nx
import community as community_louvain

from brainNetwork.util.path import get_local_datadir, get_fig_dir

csv_dir = Path(get_local_datadir(),'connectivity_rinse.csv')
connect_mat = pd.read_csv(csv_dir, index_col=0)
# Get the graph
G = nx.convert_matrix.from_pandas_adjacency(connect_mat)
# Partition
partition_louvian = community_louvain.best_partition(G, random_state=1)
print(f"The modularity is: {community_louvain.modularity(partition_louvian,G)}")
# Make the dict into a data frame
partition_pd = pd.DataFrame(partition_louvian, index = ['partition'])
assert all(partition_pd.columns == connect_mat.columns)
# Write to CSV file
#partition_pd.to_csv(os.path.join(get_local_datadir(), 'partition.csv'))
# reorder columns and rows
reorder_indices = partition_pd.columns[np.squeeze(np.argsort(partition_pd.values))]
connect_mat = connect_mat.reindex(columns = reorder_indices, index = reorder_indices)
# Plot the 
fig = plt.figure(figsize=(7, 7))
plt.imshow(connect_mat.values, cmap='gray_r', interpolation='none', vmin = 0, vmax=1)
num_rois = connect_mat.shape[0]
plt.xticks(np.arange(num_rois), connect_mat.columns,rotation=90)
plt.yticks(np.arange(num_rois), connect_mat.columns,rotation=0)
fig_name = Path(get_fig_dir(), 'the_matrix_rinse.svg')
fig.savefig(fig_name)
pass
#plt.colorbar()
## draw the graph
#pos = nx.spring_layout(G)
## color the nodes according to their partition
#cmap = cm.get_cmap('viridis', max(partition_louvian.values()) + 1)
#nx.draw_networkx_nodes(G, pos, partition_louvian.keys(), node_size=40,
#                       cmap=cmap, node_color=list(partition_louvian.values()))
#nx.draw_networkx_edges(G, pos, alpha=0.5)
#plt.show()