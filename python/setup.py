from setuptools import setup

setup(name='brainNetwork',
      version='0.1',
      description='Utility tools for brain network connetivity analysis using fMRI data',
      url='https://gitlab.mpcdf.mpg.de/alik/Brain_network/',
      author='Ali Karimi, Renee Hartig',
      author_email='ali.karimi@brain.mpg.de',
      packages=['brainNetwork']
      )
