function [volume] = volume_ROI()
%volume_ROI Load volume ROIs
m = load(fullfile(util.get_paths().data_versioned, ...
    'region_info', 'volume.mat'));
volume = m.volume;
end

