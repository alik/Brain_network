% Get the number of sessions per animal segmented by the taste quality
util.clearAll;
cur_save_dir = fullfile(util.get_paths().figs,'betas_taste_animal');
util.mkdir(cur_save_dir)
% Get the beta tables from matfiles
beta_tables_taste = util.load.all_taste_betas().combined;

%% return locations of which set for all rows in s
X = beta_tables_taste(:,{'only_animal_idx', 'tastant'});
s = X.Variables;
[~,ia,ic]=unique(s,'rows');
n = histc(ic,unique(ic));
T = table(s(ia,1),s(ia,2),n);
% unstack the tabe
T_unstacked = unstack(T,'n','Var2');
T_unstacked.Properties.RowNames = cellstr(T_unstacked.Var1);
T_unstacked.Var1 = [];

% convert nans to zeros
var_vals = T_unstacked.Variables;
var_vals(isnan(var_vals)) = 0;
T_unstacked.Variables = var_vals;
% sort animals
T_unstacked = sortrows(T_unstacked,1,'descend');

%% Stacked bar plot
fh = figure; ax = gca;
fractions = (T_unstacked.Variables./sum(T_unstacked.Variables,2))*100;
ba = bar(fractions,...
    'stacked','FaceColor','flat');
colors = util.plot.getColors().taste;

for i=1:length(ba)
    ba(i).CData = colors(i,:);
end
xticklabels(T_unstacked.Properties.RowNames)

util.plot.cosmeticsSave...
    (fh, ax, 3.2, 1.4, cur_save_dir,...
     'barplot_fractions.svg', 'off','off');
%% Save the table
save_mat = false;
if save_mat
    save(fullfile(cur_save_dir, 'betas_taste_animal.mat'), 'T_unstacked');
end

%% IGNORE(USE PYTHON chi_square_taste_fractions.py): Chi square proportions test
% first calculated expected numbers for each animal based on the sum of the
% session numbers
total_proportions = sum(T_unstacked.Variables,1)./...
    sum(T_unstacked.Variables,'all');
observed_per_animal = T_unstacked.Variables;
expected_per_animal = sum(T_unstacked.Variables,2) * total_proportions;
num_animals = length(observed_per_animal);

for i = 1:num_animals
    [h(i),p(i),stats] = chi2gof([1,2,3],'Frequency',observed_per_animal(i, :),...
        'Expected',expected_per_animal(i, :),'Alpha',0.05);
    disp(stats)
end
%% Write the csv for python
% NOTE: matlab automatically pools the data from multiple taste groups when
% the n is small for the last three animals, use python which does not pool
% automatically (chi_square_taste_fractions.py)
writetable(array2table(observed_per_animal,...
    'VariableNames',T_unstacked.Properties.VariableNames,...
    'RowNames',T_unstacked.Properties.RowNames),...
    fullfile(cur_save_dir, 'observed.csv'),'WriteRowNames',true)
writetable(array2table(expected_per_animal,...
    'VariableNames',T_unstacked.Properties.VariableNames,...
    'RowNames',T_unstacked.Properties.RowNames),...
    fullfile(cur_save_dir, 'expected.csv'),'WriteRowNames',true)
