function [min_max_vals] = min_max(this_array)
%MIN_MAX get the minimum and maximum of an array
lin_array = this_array(:);
min_max_vals = [min(lin_array), max(lin_array)];
end

