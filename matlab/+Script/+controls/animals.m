%% Script to compare the beta values in relation to the animals used in each experiment
% Rinse betas indices all_beatas 31:2:59
util.clearAll;
outdir = fullfile(util.get_paths().figs,'controls', 'animals');
util.mkdir(outdir)

beta_tbl = util.load.beta_table();

%% Split data by animal identity
[G, TID] = findgroups(beta_tbl(:, 'animal_index'));
per_animal = ...
    splitapply(@(x,y) {x(:),y(:)}, ...
    beta_tbl(:, {'rinse_Betas','high_low_betas'}), G);
per_animal = per_animal';
per_animal = util.padcat(per_animal{:});

%% Box plot

% sort by the interquartile range 
[~, I] = sort(iqr(per_animal));

x_labels = ...
    Script.controls.label_names(cellstr(TID{:,:}), {'rinse', 'tastant'});
x_labels = x_labels(I);
% Note: outliers are not displayed with 'symbol' being empty char
fh = figure(); ax = gca;
boxplot(per_animal(:,I), 'Jitter', 1,'symbol', 'x',...
    'PlotStyle', 'traditional')
set(ax, 'XTick', 1:length(x_labels), 'XTickLabel', x_labels)
ylim([-5,5])
xlabel('Animal idx')
ylabel('Beta values')
