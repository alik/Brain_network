function [consistent_names] = check_ROI_name_consistency(beta_tables, indices)
%CHECK_ROI_NAMES make sure all sessions have the same ROI names
% INPUT:
%   beta_tables: cell array of tables of the beta values for each session
%   indices: the indices of the sessions to compare names
% OUTPUT:
%   consistent_names: The consistent ROI names
if ~exist('indices','var') || isempty(indices)
    indices = 1:length(beta_tables);
end
session_ROIs = arrayfun(@(x) beta_tables{x}.Properties.RowNames,...
    indices, 'UniformOutput', false);

% Compare between equal sized ROIs
assert(isequal(session_ROIs{:}), 'Incosistent region names');

% Now that we have seen all ROIs have the same names we return the first
% one
consistent_names = session_ROIs{1};
end

