util.clearAll;
beta = util.load.beta_table();

%% Get the first three letters representing the animal
only_animal_idx = char(beta.animal_idx);
only_animal_idx = categorical(cellstr(only_animal_idx(:,1:3)));
beta = addvars(beta,only_animal_idx,'Before','animal_idx');

%% write beta table to mat and csv files
outdir = fullfile(util.get_paths().data_versioned, 'betas');
final_beta_table = beta;
save(fullfile(outdir, 'full_beta_tbl.mat'), 'final_beta_table')
f_name = fullfile(util.get_paths().data_versioned, 'betas', 'full_beta_tbl.csv');
writetable(beta, f_name)