function [regions_T] = select_regions(T, region_names, hemisphere)
%SELECT_REGIONS select a set of regions within a hemisphere in the beta
%data table
% INPUT:
%   T: the beta data table with region_names and hemisphere information
%   region_names: a cell string array with the name and order of ROIs
%   hemisphere: the hemisphere
% OUTPUT:
%   updated_T
idx = ismember(T.hemisphere, hemisphere) & ...
    ismember(T.region_name,region_names);
regions_T = T(idx, :);
[~, reorder_idx] = ismember(region_names, cellstr(regions_T.region_name));
regions_T = regions_T(reorder_idx,:);
end

