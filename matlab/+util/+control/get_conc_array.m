function [high_low_betas] = get_conc_array(tbl)
%GET_CONC_ARRY Concatenate the high and low taste betas into an array
high_low_betas = cellfun(@(x) {x.high_Betas(:), x.low_Betas(:)}, ...
    tbl.beta_table, 'UniformOutput', false);
% turn it into an array
high_low_betas = cat(1, high_low_betas{:});
high_low_betas = ...
    arrayfun(@(x) cat(1, high_low_betas{:,x}), 1:2, 'UniformOutput', false);
high_low_betas = arrayfun(@(x) cat(2, high_low_betas{x}{:})', 1:2,...
    'UniformOutput', false);
end

