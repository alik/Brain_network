import os
from pathlib import Path

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

import networkx as nx
from brainNetwork.util.path import get_local_datadir, get_fig_dir
from brainNetwork.util import connectome, util
import itertools
from scipy.stats import ttest_ind
# Paths
csv_dir = Path(get_local_datadir(),'connectivity_tastant.csv')
save_dir = Path(get_local_datadir(), 'shuffled_modularity')
save_dir.mkdir(exist_ok=True)

# Read the matrix
connect_mat = pd.read_csv(csv_dir, index_col=0)

# Get the graph and edge list
G = nx.convert_matrix.from_pandas_adjacency(connect_mat)
df_pre_post = nx.to_pandas_edgelist(G)

# calculate and save permutated modularities
get_shuffling = False
if get_shuffling:
    modularities = connectome.permute_modularity(pre_post_list=df_pre_post, 
                                                output_path=save_dir)

# Get the observed modularity 
dir_csv = Path(get_local_datadir(), 'repeat_modularity','freq_modularity.csv')
freq_modularity = pd.read_csv(dir_csv)
all_mods = {((0,0,0),350):freq_modularity.modularities.values}
# Get the folder of each modularity run (per taste and combined)
df_f_paths = connectome.get_per_taste_folder(f_name='freq_modularity.csv')
# Get the modularites from the paths
df_f_paths['modularities'] = df_f_paths['paths'].apply(lambda x: pd.read_csv(x).modularities.values)
# Add ypos
df_f_paths['ypos'] = np.hstack([np.arange(0,-121,-40)+350,np.arange(0,-121,-40)+150])
# Add colors
colors_for_plot = np.tile(np.vstack((util.colors_taste(), [0.5, 0.5, 0.5])),(2,1))
# Plot them
fig, ax = plt.subplots(figsize=util.cm2inch(3.3,1.8))
connectome.plot_histogram(df_real_modularity=df_f_paths,
                          path_modularity_file=save_dir,
                          scatter_colors = colors_for_plot)
                         

# TTEST modularity taste and rinse
tastants_mod = np.asarray(list(itertools.chain(*df_f_paths['modularities'][0:4])))
rinse_mod = np.asarray(list(itertools.chain(*df_f_paths['modularities'][4:])))
result = ttest_ind(tastants_mod, rinse_mod)
pass