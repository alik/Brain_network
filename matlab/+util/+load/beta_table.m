function [beta_table] = beta_table()
%BETA_TABLE Load beta table MAT file
%   OUTPUT:
%       beta_table: The loaded table of beta values
m = load(fullfile(util.get_paths().data_versioned, ...
    'betas', 'full_beta_tbl.mat'));
beta_table = m.final_beta_table;
end

