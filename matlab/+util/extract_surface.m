function [iso_surfaces_native] = extract_surface(volumes)
%EXTRACT_SURFACE extract surfaces from a cell of volumes

% create empty table to save final meshes
iso_surfaces_native = util.create_empty_table(volumes);

% extract isosurface
iso_surfaces = cellfun(@(x) isosurface(x, 0), volumes.Variables, ...
    'UniformOutput', false);

% scale to voxel size and move by dataset offset
voxel_size = util.get_param().voxel_size;
off_set = util.get_param().offset;
iso_surfaces_native.Variables = cellfun(...
    @(x) struct('faces', x.faces, 'vertices', ...
    (x.vertices .* voxel_size) + off_set),...
    iso_surfaces, 'UniformOutput', false);
end

