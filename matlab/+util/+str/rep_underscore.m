function [rep_strings] = rep_underscore(strings)
% REP_UNDERSCORE replace underscore(_) with a dash for variable names
% Note: variable names of tables cannot have dash but underscore is okay.
% The underscore creates issues with labeling
if iscell(strings)
rep_strings = cellfun(@(x) strrep(x, '_', '-'), strings,...
    'UniformOutput', false);
else
    rep_strings = strrep(strings, '_', '-');
end
end

