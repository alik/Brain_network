%% Setup
util.clearAll;
outdir = fullfile(util.get_paths().python_datadir,'connectivity_matrices');
util.mkdir(outdir)
c = util.plot.getColors();
beta_tables_taste = util.load.all_taste_betas();

%% Write combined matrices
var_strings = {'high_low_betas', 'rinse_Betas'};
save_strings = {'tastant', 'rinse'};
avg_cor_all = cell(1,2);
for i = 1:2
    cur_f_name = fullfile(outdir, ...
        sprintf('connectivity_%s_combined.csv',save_strings{i}));
    cur_var = var_strings{i};
    avg_cor_all{i} = util.IO.write_matrix_from_table(beta_tables_taste.combined, cur_var, ...
        cur_f_name);
end

%% write matrices separated by taste
taste_string = beta_tables_taste.sep_table.Properties.VariableNames;
for i = 1:2
    for t = 1:3
        cur_f_name = fullfile(outdir, ...
            sprintf('connectivity_%s_%s.csv', save_strings{i}, taste_string{t}));
        cur_var = var_strings{i};
        avg_cor = util.IO.write_matrix_from_table...
            (beta_tables_taste.sep_table{1,t}{1}, cur_var, cur_f_name);
    end
end
