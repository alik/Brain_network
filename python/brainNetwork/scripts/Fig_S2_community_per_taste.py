import os
from enum import unique
from pathlib import Path

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

import networkx as nx
from brainNetwork.util.path import get_local_datadir, get_fig_dir
from brainNetwork.util import connectome, util

# CSV file parameters
csv_main = Path(get_local_datadir(),'connectivity_matrices')
sol_str = ['tastant', 'rinse']
taste_str = ['salt', 'sour', 'sweet', 'combined']
# Output folder
save_dir_main = Path(csv_main, 'repeat_modularity')
save_dir_main.mkdir(exist_ok=True)
# Perform repetition
perform_repetition = False
# Get the order of regions from the tastant's highest modularity
df_for_order = pd.read_csv('/Users/karimia/code/Brain_Network/python/data/connectivity_matrices/repeat_modularity/connectivity_tastant_combined/unique_partitions.csv', index_col=0)


for cur_s in sol_str:
    for cur_t in taste_str:
        cur_name = f'connectivity_{cur_s}_{cur_t}'
        print(cur_name)

        # Get current file name
        cur_f_name = csv_main.joinpath(f'{cur_name}.csv')
        cur_G = connectome.get_graph_from_csv(cur_f_name)

        # Get current savedir
        cur_save_dir = save_dir_main.joinpath(cur_name)
        cur_save_dir.mkdir(exist_ok=True)
        cur_df_partitions, cur_modularities = connectome.repeat_community(cur_G, num_repeats=10000, 
                                                                          save_dir=cur_save_dir, 
                                                                          perform_repetition=perform_repetition)
        print(pd.unique(cur_df_partitions.values.max(axis=1))) #sour and sweet sometimes have only 2 clusters

        # Get consistent cluster ids
        cur_df_partitions = connectome.consistent_cluster_id(cur_df_partitions)
        cur_unique_partitions, cur_freq_modularity = connectome.get_unique_partitions(cur_df_partitions, 
                                                                                      cur_modularities,
                                                                                      region_order=df_for_order.index)

        # Round the modularities to 4 digits
        cur_freq_modularity.modularities = round(cur_freq_modularity.modularities,5)
        cur_df_rand = connectome.measure_rand_index(cur_unique_partitions)
        

        # Plot the consistency plot
        connectome.plot_consistency(cur_unique_partitions, 
                                    fig_name=Path(save_dir_main,f'consistency_{cur_name}.svg'))

        # Save the tables
        cur_freq_modularity.to_csv(Path(cur_save_dir,'freq_modularity.csv'))
        cur_unique_partitions.to_csv(Path(cur_save_dir,'unique_partitions.csv'))
        cur_df_rand.to_csv(Path(cur_save_dir,'rand_unqiue_partitions.csv'))
        print(cur_freq_modularity)