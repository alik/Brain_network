%% Set variables
clearvars -except dataDir
paradigmNum = 'reward_history_study';
SeedName = 'aPC';
numSubj = 51;
roiNum = {1, 3, 4, 5, 6, 7, 8, 10};
num_Cond = 7;
load(strcat('dataComp_condition', num2str(num_Cond)))

%% loop through the data
for seedRegion = [2,7]
    if seedRegion == 7
        roiNum{6} = 2;
        SeedName = 'Tu';
    end
    for rois = 1:length(roiNum)
        for isubj = 1:numSubj
            seed = dataComp{1, isubj}{1, seedRegion};
            roi = dataComp{1, isubj}{1, roiNum{rois}};
            corrBOLD_cat = [seed roi];
            [condCorrValues, condPVAL] = corr(corrBOLD_cat);
            individualCorrValues(isubj) = atanh(condCorrValues(2));
        end
        dataMatrix(:,rois) = individualCorrValues';
        bringBack2coeff(rois) = mean(individualCorrValues);
    end
    
    % bonferroni correct for multiple comparisons
    numComparisons = length(roiNum);
    pValue = 0.05/numComparisons;
    % compute ttest of group-level means
    [h,p,ci,stats] = ttest(dataMatrix, 0, 'Alpha', pValue);
    
    % bring back to Pearson's correlation coefficient
    Groupcoeff = tanh(bringBack2coeff);
    
    Groupcoeffcorr = Groupcoeff;
    Groupcoeffcorr(p > pValue) = 0;
    
    % save relevant outputs
    cd('/home/renee.hartig/Rodent_fMRI/results/reward_history_study/group')
    save(strcat('GroupCorr_Seed_', SeedName, '_Cond', ...
        num2str(num_Cond)), 'Groupcoeffcorr', 'p')

%% Set-up figure subplots
load(strcat('GroupCorr_Seed_', SeedName, '_Cond', num2str(num_Cond), '.mat'))
Seedresults = subplot(1,1,1);
subplotArray = [Seedresults];

%% Set-up correlograms
% set color bar limit
colorLim = [-1, 1];
% plot titles
plotTitles = {'Pearson Correlation'};
% establish array of correlation values
corrValues = {Groupcoeffcorr}';

% set initial plotsize
% compute plots
imagesc(subplotArray, corrValues{:}');
subplotArray.CLim = colorLim;
t_name = strcat(plotTitles, ' ', num2str(paradigmNum), '_', SeedName);
title(subplotArray, t_name);
% add cosmetics
[roiLabels] = cosmeticsMatrix(subplotArray, corrValues, colorLim);
% add colorbar
colorbar(subplotArray);

%% Save figure in relevant folder
saveFol = strcat(dataDir, '/Rodent_fMRI/results/', num2str(paradigmNum), ...
    '/group/', SeedName, '/ROI_correlations');

% determine which operating system you are on
if exist('C:\Program Files\MATLAB') > 0
    % set up file separator into windows format
    saveFol(strfind(saveFol,'/'))='\';
end

mkdir(saveFol)
cd(saveFol)

% Output figure
set(gcf,'PaperUnits','inches');
set(gcf,'PaperSize', [16 8]);
set(gcf,'PaperPosition',[-1 0 18 8]);
set(gcf,'PaperPositionMode','Manual');
saveas(gcf,strcat(num2str(paradigmNum), '_', SeedName, ...
    'ROI Correlogram_Cond_', num2str(num_Cond), '.pdf'))
end