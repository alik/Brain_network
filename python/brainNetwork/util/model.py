from re import I
from itertools import chain

import numpy as np
import pandas as pd

from sklearn.model_selection import train_test_split
from sklearn.inspection import permutation_importance
from sklearn.preprocessing import OneHotEncoder, MinMaxScaler
from sklearn.pipeline import Pipeline
from sklearn.compose import ColumnTransformer

def get_matlab_datadir():
    # Return the matlab data directory
    return "/Users/karimia/code/Brain_Network/data_versioned"


def remove_nan_amygdala(df_beta):
    # Remove the nan rows from the data frame
    nan_index = np.where(np.isnan(df_beta['high_low_betas_1'].to_numpy()))
    return df_beta.drop(nan_index[0])


def average_beta_cols(df_beta, col_strs: list):
    """
    Take the average of columns belonging to a specific beta type
    """
    col_idxs = [df_beta.columns.str.contains(col_name) for col_name in col_strs]
    df_averaged = df_beta.copy()
    dep_varnames = []
    for i, idx in enumerate(col_idxs):
        cur_col_names = df_beta.columns[np.where(idx)]
        dep_varnames.append(col_strs[i] + '_mean')
        df_averaged = df_averaged.assign(**{dep_varnames[i]: df_beta.loc[:, cur_col_names].mean(axis=1)})
        df_averaged.drop(cur_col_names, axis=1, inplace=True)
    
    return df_averaged, dep_varnames

def prepare_data_splits(df_beta: pd.DataFrame, indep_colnames: list, dep_varnames: list):
    """
    Create train and test split for data
    """
    X = df_beta[indep_colnames['cat'] + indep_colnames['num']]
    # Create a list of dependant variables given their names
    Y = [df_beta[[y_str]] for y_str in dep_varnames]
    # Split data into train and test. Note: random state is fixed so the same sample order for all
    split_data = [train_test_split(X, np.ravel(y), test_size=0.3, random_state=101) for y in Y]
    split_names = ["X_train", "X_test", "Y_train", "Y_test"]
    df_split = pd.DataFrame(split_data, index=dep_varnames, columns=split_names)
    return df_split

def apply_regression(data_tbl, model):
    """
    Fit the model to the data given and measure variable importance using permutation
    """
    # Fit model
    model.fit(data_tbl["X_train"], data_tbl["Y_train"])
    # Get total rsquared
    r_squared = model.score(data_tbl["X_test"], data_tbl["Y_test"])
    # Get variable importance
    perm_importance = permutation_importance(model, data_tbl["X_test"], data_tbl["Y_test"],
                                            n_repeats=40,
                                            random_state=0,
                                            n_jobs=-1)
    
    # convert to data frame
    df_reg_results = pd.DataFrame([[model, r_squared, perm_importance]], 
                                  columns=['model', 'r_squared', 'perm_importance'],
                                  index=[data_tbl.name])
    return df_reg_results

def define_reg_pipeline(reg_estimator, input_dict: dict, indep_colnames: dict):
    """
    Define the pipeline for the regression of beta values
    """
    categorical_encoder = OneHotEncoder(drop='first')
    numerical_pipe = Pipeline([
        ('min_max', MinMaxScaler(feature_range=(0, 1)))
    ])

    preprocess_cols = ColumnTransformer(
        [('cat', categorical_encoder, indep_colnames['cat']),
         ('num', 'passthrough', indep_colnames['num'])])

    pipe_reg = Pipeline([
        ('preprocess', preprocess_cols),
        ('regressor', reg_estimator(**input_dict))
    ])
    return pipe_reg

def add_permutated(df, indep_colnames:dict):
    """
    Add a permutated version of variables to the data frame
    """
    all_indep = list(chain(*indep_colnames.values()))
    for cur_indep in all_indep:
        rand_name = cur_indep + '_rand'
        df[rand_name] = np.random.permutation(df[cur_indep])
    
    with_rand_names = {k: v + [s + '_rand' for s in v] for k,v in indep_colnames.items()} 
    return df, with_rand_names