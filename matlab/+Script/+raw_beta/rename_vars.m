util.clearAll;
beta = util.load.beta_table();
beta = renamevars(beta, {'reset_session_index', 'session_index', ...
    'only_animal_idx', 'animal_idx'}, ...
    {'session_index', 'old_session_index', ...
    'animal_index', 'date_tag'});

%% write beta table to mat and csv files
outdir = fullfile(util.get_paths().data_versioned, 'betas');
final_beta_table = beta;
save(fullfile(outdir, 'full_beta_tbl.mat'), 'final_beta_table')
f_name = fullfile(outdir, 'full_beta_tbl.csv');
writetable(beta, f_name)