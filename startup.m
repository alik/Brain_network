% Large svgs are not correctly saved with the -opengl renderer
set(0, 'DefaultFigureRenderer', 'painters');

addpath(genpath('/Users/karimia/code/Brain_Network/matlab'))
addpath('/Users/karimia/code/Brain_Network/gifti')