## About Brain Network code repository:

This repository contains all the code and data used within the following manuscript:   
[**Interconnected sub-networks of the macaque monkey gustatory connectome**](TODO: XX)  
Renee Hartig, Ali Karimi, Henry C. Evrard


## How to get the code repository:
You can download the complete code repository via the Download option on top of this page (cloud icon).   
Alternatively, you can type one of the following commands in your command prompt:  
HTTPS:  
```
git clone https://gitlab.mpcdf.mpg.de/alik/Brain_network.git
```
SSH:  
```
git clone git@gitlab.mpcdf.mpg.de:alik/Brain_network.git
```
Make sure you have [git](https://git-scm.com/downloads) installed. In addition, you need to configure your 
[SSH](https://gitlab.mpcdf.mpg.de/help/ssh/README) keys in case you do not want to use HTTPS.

You can then set your matlab working directory to this code repository and run the following command:
```
>> startup
```
This should add the necessary paths to you matlab working environment.

## Requirements:
This code repository is partly written in [MATLAB](https://www.mathworks.com/) and tested using version R2021a. 
In addition to the basic matlab installation, specific segments of the code might require additional MATLAB packages.


The python code (version 3.9) is contained as a package [brainNetwork].

**installation with pip (local):**

To install the package locally run pip install in the python folder:
``` bash
pip install .
```

or via a symlink that makes any changes in the repository directly available to
all code on the system via:
``` bash
pip install -e .
```

## Setting up conda (development) enviroment

To create a conda environment for brainNetwork run the following command in the python 
 folder:
``` bash
conda env create -f environment.yml
```

Then, to activate the environment run
``` bash
conda activate brainnet
```

Additionally, the circle diagrams are written in R (version 4.0.5) using the [circlize](https://cran.r-project.org/web/packages/circlize/index.html) package.

## General purpose
The purpose of this repository is to provide an easily accessible MATLAB, Python and R interface for generation of the figures used in the manuscript.  

Each python, R or MATLAB script that generates a figure panel has the name of that panel in the beginning of the script name. For example:

**matlab/+Script/+corr/Fig_4a_correlation_visualization.m** contains the code for generating visualizations in panel **(a)** of **Figure 4**. 

## Contents
* **/MATLAB**

Contains all the matlab code used for analysis.
This code repository is organized into [MATLAB packages](https://www.mathworks.com/help/matlab/matlab_oop/scoping-classes-with-packages.html) (folders starting with "+")
To access the methods/scripts of each package you need to use dot notation. Here's some examples:
```
>> mypackage.script
>> mypackage.mysubpackage.script

```
* **/gifti**

MATLAB package to read gifti files
* **/python**

Contains all the code for the python related analysis (mostly community detection). It is organized as a python package with relevant scripts in the folder (/scripts)
* **/R**

Contains scripts for creating the circle diagrams

* **/data_versioned**

Contains the data that is kept in a versioned manner using git

* **/brainnet_data**

Contains the mat files of the beta weights (output of GLM model).

## Authors

This code repository was developed by:
* **Ali Karimi**

With significant contributions from:
* **Renee Hartig**

 
## License
This project is licensed under the [MIT license](LICENSE).
Copyright (c) 2022

