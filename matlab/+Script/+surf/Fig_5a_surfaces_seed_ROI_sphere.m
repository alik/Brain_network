%% Get the information necessary for plotting the spheres of ROIs
util.clearAll;
region_info = util.get_region_info();

% Get coordinates of the seed regions
seed_region_names = cat(2, util.str.chord_ROIs().modules{:});
seed_region_info = region_info(seed_region_names, :);
region_coords = seed_region_info.coords;
% Add right hemisphere coords as well
region_coords = [region_coords; util.flip_hemisphere_coord(region_coords)];
% Get sphere surfaces
diameter = 4;
spheres = util.make_sphere_surf(region_coords, diameter);

%% Get colors
colors = util.plot.getColors().modules;
length_mods = cellfun(@length, util.str.chord_ROIs().modules);
colors_repeated = repelem(colors, length_mods,[1,1,1]);
colors =[colors_repeated;colors_repeated];

%% Plotting: Setup
% output directory
outdir = fullfile(util.get_paths().figs, 'ROI_Spheres_chord');
util.mkdir(outdir);
% Plot the sub cortical and cortical separately
view_angles = {[-90, 0], [-180, 90], [-132, 13]};
project_type = {'orthographic','orthographic','perspective'};

%% Loop over angles/projections
for r = 1:3
    % Figure
    cur_name = ['bilateral, ',project_type{r},', ',...
        num2str(view_angles{r}, 'angles: %d, %d')];
    
    % display spheres
    t = tiledlayout(1,1,'Padding','tight');
    t.Units = 'centimeters';
    t.OuterPosition = [0.25 0.25 10 10];
    nexttile;
    set(gca, 'TickDir','out');
    util.display_multiple(spheres, colors);
    
    % Template (s)
    util.add_template('rh.gray_surface.rsl.gii', .2);
    util.add_template('lh.gray_surface.rsl.gii', .2);

    % Camera settings
    camproj(project_type{r});
    view(view_angles{r});
    camlight;
    lighting gouraud;
    
    % Save image file
    cur_save_name = fullfile(outdir, strjoin({cur_name,'png'}, '.'));
    exportgraphics(t, cur_save_name, 'Resolution', 300)
end