import os

import pandas as pd
import numpy as np
from nilearn import plotting

from brainNetwork.util.path import get_data_dir, get_fig_dir
# Note: uses the human atlas for the surface, not usable for the paper
# read coordinate files
csv_name = os.path.join(get_data_dir(), "coords", "initial.csv")
coords_pd = pd.read_csv(csv_name)
# plotting
plotting.plot_markers(node_values=np.arange(23),
                      node_coords=coords_pd.iloc[:, 0:3].to_numpy(),
                      node_size=np.ones((1, 23))*10,
                      output_file=os.path.join(get_fig_dir(), 'markers.svg'))
plotting.show()