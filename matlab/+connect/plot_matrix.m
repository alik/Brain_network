function [] = plot_matrix(avg_corr, matrix_type, plot_textStrings, colorLim)
%PLOT_MATRIX plot the matrix of connectivity
% INPUT: 
%   avg_corr: average correlation matrix
%   matrix_type: (str) type of matrix: 'tril' or 'full'
arguments
    avg_corr 
    matrix_type 
    plot_textStrings = true
    colorLim = [0, 1]
end
imagesc(avg_corr.Variables, colorLim);
colorbar();
connect.cosmeticsMatrix(avg_corr.Variables, colorLim, matrix_type, plot_textStrings);

% number of regions
xticks(1:width(avg_corr));
yticks(1:height(avg_corr));
x_labels = util.str.rep_underscore(avg_corr.Properties.VariableNames);
y_labels = util.str.rep_underscore(avg_corr.Properties.RowNames);
xticklabels(x_labels);
yticklabels(y_labels);
ytickangle(-45);
xtickangle(-45);

% Save image
daspect([1,1,1]);
end