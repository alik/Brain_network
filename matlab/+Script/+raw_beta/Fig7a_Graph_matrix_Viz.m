util.clearAll;
% stimuli
outdir = fullfile(util.get_paths().figs,'full_matrix');
util.mkdir(outdir)

%% load the data and combine betas
% Get the beta tables from matfiles
taste_str = util.str.taste_strings();
beta_tables_taste = ...
    cellfun(@connect.read_matfiles_taste, taste_str, ...
    'UniformOutput', false);
combined_betas = cat(1, beta_tables_taste{:});

%% Get the correlations
matrix_type = 'full';
hemispheres = {{'L', 'R'}, {'L', 'R'}};
cur_name = 'Combined-taste-full-matrix';
% Correlations: get the average correlations
[avg_corr, correlations] = ...
    connect.correlations(combined_betas, hemispheres,'varname_str');
% ttest for significant correlations
[ttest_results, ttest_p] = ...
    connect.ttest(correlations, matrix_type);
assert(all(cell2mat(ttest_results), 'all'));
% correlogram
fh = figure('Name',['Connectivity Matrix: ',...
    cur_name]);
ax = gca;
title(cur_name)
cur_save_name = fullfile(outdir, strjoin({cur_name,'pdf'}, '.'));
connect.plot_matrix(avg_corr.(matrix_type), matrix_type,false);
util.plot.box_ticks(ax)
set(ax,'fontname', 'Arial')
xticklabels([])
yticklabels([])
colorbar off
axis off
util.plot.cosmeticsSave(fh, ax, 2, 2, outdir,...
     strjoin({cur_name,'svg'}, '.'), 'off','off', true);
%% Get graph
c = util.plot.getColors();
G_corr = graph(avg_corr.(matrix_type).Variables, avg_corr.(matrix_type).Properties.RowNames, ...
    'lower', 'omitselfloops');
LWidths = 0.2 * G_corr.Edges.Weight/max(G_corr.Edges.Weight);
fh = figure('Name',['Connectivity Graph: ',...
    cur_name]);
ax = gca;
G_corr.plot('LineWidth',LWidths,'EdgeColor',c.gray, 'NodeLabel',[],'Layout', 'force');

util.plot.cosmeticsSave(fh, ax, 8, 8, outdir,...
     strjoin({'Graph','svg'}, '.'), 'off','off', true);