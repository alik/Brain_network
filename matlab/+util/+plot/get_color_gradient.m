function [colors_p] = get_color_gradient(c_1, c_2, length)
%GET_COLOR_GRADIENT get gradient between colors c_1 and c_2
colors_p = [linspace(c_1(1),c_2(1),length)', ...
    linspace(c_1(2),c_2(2),length)', ...
    linspace(c_1(3),c_2(3),length)'];

end

