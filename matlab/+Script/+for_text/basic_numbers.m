%% First get number of animals
beta = util.load.beta_table();

unique_animal_indices = unique(beta.only_animal_idx);
unique_session_indices = unique(beta.session_index);
