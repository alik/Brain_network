function [corrValues] = runCorrelations(corrBOLD_cat, paradigmNum, counter, dataDir)
% This function calculates the pairwise correlation between
% regions-of-interest (ROIs) as well as the partial correlations
%
% Compiler(s): Renee Hartig (renee.hartig@tuebingen.mpg.de) & Ali Karimi
% (ali.karimi@brain.mpg.de)
%
% Inputs:
% corrBOLD_cat % concatenated regional BOLD correlations
% paradigmNum % string containing paradigm information, e.g. 'P2'
% counter % counter to go through multiple conditions
% dataDir % directory where data is located, an output from startup.m
%
%% Calculate condition BOLD correlation
[condCorrValues, condPVAL] = corr(corrBOLD_cat);
% Set the diagonal to zero
condCorrValues = condCorrValues-diag(diag(condCorrValues));
% Correct for multiple comparisons
correctedCondCorr = condCorrValues;
edgeLength = length(condCorrValues);
numComparisons = (edgeLength * (edgeLength-1))/2;
correctedCondCorr(condPVAL > 0.05/numComparisons)=0;
% Take only the lower half
condCorrOutput = tril(correctedCondCorr);

% Calculate condition BOLD partial correlation
[condpCorrValues, condPartialPVAL] = partialcorr(corrBOLD_cat);
% Set the diagonal to zero
condpCorrValues = condpCorrValues-diag(diag(condpCorrValues));
% Correct for multiple comparisons
correctedCondpCorr = condpCorrValues;
% Already have edge length computed
numComparisons = (edgeLength * (edgeLength-1))/2;
correctedCondpCorr(condPartialPVAL > 0.05/numComparisons)=0;
% Take only the lower half
condpCorrOutput = tril(correctedCondpCorr);

%% Set-up figure subplots
Corr = subplot(2,2,1);
pCorr = subplot(2,2,2);
subplotArray = [Corr pCorr];

%% Set-up correlograms
% set color bar limit
colorLim = [-1, 1];
% plot titles
plotTitles = {'Correlation' 'Partial Correlation'};
% establish array of correlation values
corrValues = {condCorrOutput condpCorrOutput};

% set initial plotsize
% compute plots
for i = 1:length(subplotArray)
    imagesc(subplotArray(i), corrValues{i});
    subplotArray(i).CLim = colorLim;
    t_name = strcat(plotTitles(i), ' ', num2str(paradigmNum), ' ROI Correlogram');
    title(subplotArray(i), t_name);
    % add cosmetics
    [roiLabels] = cosmeticsMatrix(subplotArray(i), corrValues(i), colorLim);
    % add colorbar
    colorbar(subplotArray(i));
end

%% Save figure in relevant folder
saveFol = strcat(dataDir, '/Rodent_fMRI/results/', num2str(paradigmNum), '/group/ROI_correlations');

% determine which operating system you are on
if exist('C:\Program Files\MATLAB') > 0
    % set up file separator into windows format
    saveFol(strfind(saveFol,'/'))='\';
end

mkdir(saveFol)
cd(saveFol)

% Output figure
set(gcf,'PaperUnits','inches');
set(gcf,'PaperSize', [16 8]);
set(gcf,'PaperPosition',[-1 0 18 8]);
set(gcf,'PaperPositionMode','Manual');
saveas(gcf,strcat(num2str(paradigmNum), ' Condition ', num2str(counter), ' Group ROI Correlogram.pdf'))

%% plot representative correlation and anticorrelation plots
% [maxFig minFig] = correlationPlots(corrBOLD_cat, corrValues, roiLabels);
% % Output figures
% for i = 1:length(maxFig)
% print(maxFig(i), strcat(num2str(paradigmNum), ' Condition ', num2str(counter), plotTitles{i},' Maximum'), '-dsvg');
% print(minFig(i), strcat(num2str(paradigmNum), ' Condition ', num2str(counter), plotTitles{i},' Minimum'), '-dsvg');
% end
% 
% plot connectivity graphs 
[h] = connectivityGraphs(condCorrOutput, roiLabels);
saveas(h, strcat(num2str(paradigmNum), ' Condition ', num2str(counter), ' Connectivity Graph PartialCorr.svg'));
end