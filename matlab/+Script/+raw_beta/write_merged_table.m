util.clearAll;
outdir = fullfile(util.get_paths().data_versioned, 'betas');
util.mkdir(outdir)
%% load the data and combine betas
% Read beta values and calculate their correlation
beta_tables_taste = util.load.all_taste_betas();
beta_merged = connect.preprocess.merge_into_large_tbl...
    (beta_tables_taste.sep_table);
final_beta_table = cat(1, beta_merged.single{1,:}{:});

%% save final table as mat file
save(fullfile(outdir, 'full_beta_tbl.mat'), 'final_beta_table')