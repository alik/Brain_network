function [yfit,X, P] = fit_linear(X, Y)
%LINEAR_FIT linear fit to the data
% remove the nans
ind = isnan(X) | isnan(Y);
X(ind) = [];
Y(ind) = [];
% fit to the remaining data
P = polyfit(X, Y,1);
fprintf('The linear fit: Y = %f * X + %f \n',round(P,5))
yfit = P(1)*X+P(2);
end

