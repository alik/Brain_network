# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%
from IPython import get_ipython

# %%
# General imports
import os

import numpy as np
import pandas as pd

import seaborn as sns
import matplotlib.pyplot as plt

import statsmodels.api as sm

from IPython.display import display
#get_ipython().run_line_magic('load_ext', 'autoreload')
#get_ipython().run_line_magic('autoreload', '2')

# Saving text not as a path
import matplotlib as mpl
new_rc_params = {'text.usetex': False,
"svg.fonttype": 'none'
}
mpl.rcParams.update(new_rc_params)


# %%
# Local imports
from brainNetwork.util.path import get_fig_dir
from brainNetwork.util.model import get_matlab_datadir, remove_nan_amygdala, average_beta_cols, prepare_data_splits, apply_regression, define_reg_pipeline, add_permutated


# %%
# Read beta table
f_name = os.path.join(get_matlab_datadir(), 'betas', 'full_beta_tbl.csv')
df_beta = pd.read_csv(f_name)
# Remove amygdala rows with Nan values
df_beta = remove_nan_amygdala(df_beta)


# %%
# Options for pandas display
pd.options.display.max_rows = 100
pd.options.display.max_columns = 100


# %%
# Average dependant values 
col_strs = ['all_Betas','high_low_betas','high_Betas', 'low_Betas', 'rinse_Betas']
df_beta_new, dep_varnames = average_beta_cols(df_beta, col_strs=col_strs)


# %%
# Plot the line plot of the session index
# set style to ticks to match matlab figures
sns.set_style("ticks")
fig, cur_ax = plt.subplots(1,1,figsize=[5, 5])
cur_ax = sns.lineplot(x='session_index', 
            y='rinse_Betas_mean',
            data=df_beta_new, 
            ax=cur_ax,
            color='gray',
            ci="sd")
cur_ax = sns.lineplot(x='session_index', 
            y='high_low_betas_mean',
            data=df_beta_new, 
            ax=cur_ax,
            color='magenta',
            ci="sd")
cur_ax.set_xlabel('Session order')
cur_ax.set_ylabel('Beta values')
cur_ax.get_xaxis().set_visible(True)
cur_ax.set_ylim([-1.5,1.5])
sns.despine()

plt.show()
fig_name = os.path.join(get_fig_dir(), 'session_index_lineplots.svg')
fig.savefig(fig_name)


# %%
# Create a stacked variable for the rinse vs tastant betas for animal boxplots and the Taste Quality plots below
rinse_vs_taste = df_beta_new.rename(columns={"high_low_betas_mean": "Tastant",
"rinse_Betas_mean": "Rinse"}).melt(id_vars = ['session_index', 'animal_index','date_tag','taste', 'region_name', 'hemisphere'], 
                                  value_vars=['Rinse','Tastant'],
                                  var_name = 'solution',
                                  value_name='beta')
rinse_vs_taste

rinse_vs_high_vs_low = df_beta_new.rename(columns={"high_Betas_mean": "High", "low_Betas_mean": "Low", "rinse_Betas_mean": "Rinse"}).melt(
                                  id_vars = ['session_index', 'animal_index','date_tag','taste', 'region_name', 'hemisphere'], 
                                  value_vars=['Rinse','High', 'Low'],
                                  var_name = 'solution',
                                  value_name='beta')

len(rinse_vs_high_vs_low.date_tag.unique())


# %%
# Standard deviations for reporting
rinse_vs_high_vs_low.groupby(['solution']).std()


# %%
fig, cur_ax = plt.subplots(1,1,figsize=[5, 5])
cur_ax = sns.boxplot(x='animal_index', 
            y='beta' ,
            hue='solution',
            palette=['grey', 'magenta'],
            data=rinse_vs_taste, 
            ax=cur_ax,
            showfliers=False)

cur_ax.set_ylim([-2,2])
cur_ax.set_xlabel('Animal index')
cur_ax.set_ylabel('Average beta values')
# Remove the box
sns.despine()
plt.show()
fig_name = os.path.join(get_fig_dir(), 'animal_beta_boxplots.svg')
fig.savefig(fig_name)


# %%



# %%
# Visualize 
fig, (ax_tastant, ax_rinse) = plt.subplots(2,1,figsize=[15, 15])
sns.boxplot(x='region_name', 
            y='rinse_Betas_mean', 
            hue="taste",
            palette=['pink','deepskyblue','yellow'],
            data=df_beta_new, 
            ax=ax_rinse,
            showfliers=False)
ax_tastant.tick_params(axis='x', rotation=90)
ax_tastant.get_xaxis().set_visible(False)
ax_tastant.set_ylim([-2,2])
sns.boxplot(x='region_name',
            y='high_low_betas_mean',
            palette=['pink','deepskyblue','yellow'],
            hue="taste", 
            data=df_beta_new, 
            ax=ax_tastant,
            showfliers=False)
ax_rinse.tick_params(axis='x', rotation=90)
ax_rinse.set_ylim([-2,2])
sns.despine()
plt.show()
fig_name = os.path.join(get_fig_dir(), 'taste_by_region.svg')
fig.savefig(fig_name)


# %%
# Average over regions for both rinse and taste betas and box plot
fig, ax = plt.subplots(figsize=[10, 10])
vars_interest = ['taste', 'region_name', 'high_low_betas_mean', 'rinse_Betas_mean']
means = df_beta_new.groupby(by=['region_name', 'taste'],sort=False).mean().reset_index()
means = means[vars_interest]
rinse_means = means.iloc[:,[0,1,3]].rename(columns={'rinse_Betas_mean':'Average beta over regions'}).assign(solution='Rinse')
taste_means = means.iloc[:,[0,1,2]].rename(columns={'high_low_betas_mean':'Average beta over regions'}).assign(solution='Tastant')

forplot = pd.concat([rinse_means,taste_means],axis=0)
sns.boxplot(data=forplot,
            x='solution', 
            hue='taste',
            y='Average beta over regions', 
            palette=['pink','deepskyblue','yellow'],
            showfliers=False)
sns.despine()
fig_name = os.path.join(get_fig_dir(), 'violin_Taste_vs_rinse_average_region_beta.svg')
plt.ylim([-0.3, 0.3])
fig.savefig(fig_name)


# %%
# Plot beta values separated by rinse and tastant not averaged over regions
fig, ax = plt.subplots(figsize=[10, 10])
sns.boxplot(data=rinse_vs_taste,
            x='solution', 
            hue='taste',
            y='beta', 
            palette=['pink','deepskyblue','yellow'],
            #palette=['red', 'cyan'],
            showfliers=False,
            order=['Rinse', 'Tastant'])
fig_name = os.path.join(get_fig_dir(), 'Taste_vs_rinse_by_tastant.svg')
sns.despine()
plt.ylim([-2, 2])
fig.savefig(fig_name)


# %%
# Test for the difference in beta values of tastant vs rinse
from scipy.stats import ttest_ind

gb = rinse_vs_taste.groupby(['taste', 'solution'])
gb = dict(list(gb))

taste_str = ['sour', 'salt', 'sweet']
test_results = []
for t in taste_str:
    cur_taste_idx = [key for key in gb.keys() if key[0] == t]
    idx_rinse, idx_taste = cur_taste_idx
    test_results.append(ttest_ind(gb[idx_rinse]['beta'], gb[idx_taste]['beta']))

display(pd.DataFrame(test_results, index=taste_str))


# %%
rinse_vs_taste


# %%
# The name of categorical and numerical independant variable columns
indep_colnames = {'cat': ['taste', 'animal_index', 'date_tag', 'hemisphere', 'region_name','solution'], 'num': ['session_index']}
rinse_vs_taste, indep_colnames_withrand = add_permutated(df=rinse_vs_taste, indep_colnames=indep_colnames)
df_split = prepare_data_splits(df_beta=rinse_vs_taste, indep_colnames=indep_colnames, dep_varnames=['beta'])


# %%
# Split Y and X: X_train, X_test, y_train, y_test
from sklearn.linear_model import LinearRegression
from sklearn.neural_network import MLPRegressor
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor

reg_constructor = RandomForestRegressor
input_dict = {"n_estimators": 100, "n_jobs": -1, "oob_score": True, "bootstrap": True,"random_state": 42}
#reg_constructor = LinearRegression
#input_dict = {}

reg_results = []
# Loop over rows with different beta values
for _,cur_data in df_split.iterrows():
    # Define current pipeline to fit the row of data splits
    cur_pipe_reg = define_reg_pipeline(reg_estimator=reg_constructor, 
                                       input_dict=input_dict,
                                       indep_colnames=indep_colnames)
    # Apply the regression
    cur_reg_results = apply_regression(data_tbl=cur_data, model=cur_pipe_reg)
    reg_results.append(cur_reg_results)

reg_results = pd.concat(reg_results)
# See r_squared
display(reg_results)


# %%



# %%
result.importances[sorted_idx]


# %%
# Plot the permutation importance for the different dependant variables
indep_all = np.asarray(sum(indep_colnames.values(),[]))

for _, cur_row in reg_results.iterrows():   
    result = cur_row.perm_importance
    sorted_idx = result.importances_mean.argsort()

    fig, ax = plt.subplots(figsize=[4,3])
    ax.boxplot(result.importances[sorted_idx].T,
            vert=False,widths=1, labels=indep_all[sorted_idx])
    ax.set_xlabel("Independent variables")
    ax.set_ylabel(f"Reduction in R2 after permutation")
    ax.set_xticks([0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8])
    ax.tick_params(axis='x', rotation=0)
    fig.tight_layout()
    sns.despine()
    plt.show()
    fig_name = os.path.join(get_fig_dir(), 'permutation_random_Forest_R^2_0.4299.svg')
    fig.savefig(fig_name)


# %%
# Plot correlation between the rinse and the tastant betas
fig, ax = plt.subplots(figsize=[10, 10])
sns.histplot(x='low_Betas_mean',
            y='high_Betas_mean',
            data=df_beta_new,
            ax=ax,
            cbar=True)
plt.axis('scaled')
plt.xlim([-4,4])
plt.ylim([-4,4])


fig_name = os.path.join(get_fig_dir(), 'histogram_high_vs_low.svg')
fig.savefig(fig_name)
# corr = -0.001815
df_beta_new.corr()


