import os
from enum import unique
from pathlib import Path

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

import networkx as nx
from brainNetwork.util.path import get_local_datadir, get_fig_dir
from brainNetwork.util import connectome, util

# Paths
csv_dir = Path(get_local_datadir(),'connectivity_tastant.csv')
save_dir = Path(get_local_datadir(), 'repeat_modularity')
save_dir.mkdir(exist_ok=True)
# Read the matrix
connect_mat = pd.read_csv(csv_dir, index_col=0)
# Get the graph
G = nx.convert_matrix.from_pandas_adjacency(connect_mat)

# Perform repeated communities with different seeds
perform_repetition = False
df_partitions, modularities = connectome.repeat_community(G, num_repeats=10000, 
                                                          save_dir=save_dir, 
                                                          perform_repetition=perform_repetition)
assert (df_partitions.values.max(axis=1) == 2).all() # Always only 3 clusters


# create consistent indices
df_partitions = connectome.consistent_cluster_id(df_partitions)

# Make sure the same modularity means the same clustering
[vals, idx, counts] = np.unique(modularities.values, return_index=True, return_counts=True)
for val in vals: 
    cur_same_mod = df_partitions.loc[modularities.values == val,:]
    assert cur_same_mod.eq(cur_same_mod.iloc[0, :], axis=1).all().all()

# Measure randomness-adjusted rand score between unique partitions
unique_partitions = df_partitions.iloc[idx,:]
df_rand = connectome.measure_rand_index(unique_partitions)
# turn index into frequency
fractions = (counts*100) / counts.sum()
unique_partitions.index = fractions
# Save fractions and their modularities
frac_modularities = pd.DataFrame((fractions, vals), index = ['fractions', 'modularities']).transpose()
frac_modularities.to_csv(Path(save_dir,'freq_modularity.csv'))

# Read paper partition and add
# csv_dir = Path(get_local_datadir(),'partition_paper.csv')
# partition_paper = pd.read_csv(csv_dir,sep=';')
# partition_paper.index = ['partition_paper']
# unique_partitions = pd.concat([unique_partitions, partition_paper])

# Save unique partitions
unique_partitions_tosave = unique_partitions.copy()
unique_partitions_tosave = unique_partitions_tosave.sort_values(by=[10.36], axis=1)
unique_partitions_tosave.to_csv(Path(save_dir,'unique_partitions.csv'))

# Plot
fig, ax = plt.subplots(1, 1, figsize=[4, 8])
c_map_mods = util.colors()
sorted_partition = unique_partitions.sort_values(by=[10.36], axis=1)
sorted_partition = sorted_partition.T
shape_part = sorted_partition.shape

plt.imshow(c_map_mods[sorted_partition.values])
plt.yticks(range(shape_part[0]), sorted_partition.index)
plt.xticks(range(shape_part[1]), fractions, rotation=90)
fig_name = Path(get_fig_dir(), 'the_communities_repetition_tastant.svg')
fig.savefig(fig_name)