function [] = add_template(name, alpha)
%ADD_TEMPLATE add the template given the name
if ~exist('alpha','var') || isempty(alpha)
    alpha = 1;
end
f_name = fullfile(util.get_data_dir, ...
    'template_surfaces', name);
this_surface = util.get_surface_from_gifti(f_name);
color = [244,229,206] ./ 255;
util.display_surface(this_surface, color, alpha);
end

