function [idx_by_module, module_idx] = sort_by_module(region_names)
%SORT_BY_MODULE 
region_names = region_names(:)'; % get the row vector
module_idx = modularity.get_frequent_partition();
% Sort the order of the regions in the modules to match the matrix
[~,idx] = ismember(region_names, ...
    module_idx.Properties.VariableNames);
module_order_match = module_idx(:,idx);
% make sure the order of ROIs are the same
assert(isequal(region_names, ...
    module_order_match.Properties.VariableNames))
% Get sorted indices
[~, idx_by_module] = sort(module_order_match.Variables);

% ordered module idx
module_idx = module_order_match(:,idx_by_module);
end

