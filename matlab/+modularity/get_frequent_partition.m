function [most_consistent] = get_frequent_partition()
%GET_FREQUENT_PARTITION Summary of this function goes here
%   Detailed explanation goes here
% read the communities
module_csv = fullfile(util.get_paths().python_datadir, 'repeat_modularity',...
    'unique_partitions.csv');
module_idx = readtable(module_csv,'ReadRowNames',true);
% Chose the one with 10% as the best partition since it only has 1
% difference with 89% one which is the L_NAC being in the subcortical
% module
most_consistent = module_idx(6,:);
assert(issorted(most_consistent.Variables))
end

