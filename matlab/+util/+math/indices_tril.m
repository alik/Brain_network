function [indices_lower] = indices_tril(X)
% INDICES_TRIL 
this_size = size(X);
% select the lower triangular without the diagonal
diagonal = -1;
dim_1 = 1:this_size(1);
dim_2 = 1:this_size(2);
B = rot90(bsxfun(@plus,dim_1,dim_2(:)))-1;
indices_lower = B <= (this_size(1) + diagonal);

% if the given matrix is 3D, replicate along the 3rd dimension
if numel(this_size) == 3
    indices_lower = repmat(indices_lower, 1, 1, this_size(3));
end
end

