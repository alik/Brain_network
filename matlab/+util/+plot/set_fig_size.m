function [fh] = set_fig_size(fh, dimensions)
%SET_FIG_SIZE set the units to centimeters and then adjust the dimensions
%of the figure
fh.Units = 'centimeters';
fh.Position = [10, 10, dimensions];

end

