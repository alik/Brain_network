function [p_anova_corr, pvals_T] = anova1(cur_split, taste_names, stub_rows)
%ANOVA1
arguments
    cur_split
    taste_names
    stub_rows = ''
end

g_ids = cellfun(@(taste_names, beta_array) repmat(categorical({taste_names}), ...
    length(beta_array), 1), taste_names, cur_split,'UniformOutput', false);
cur_split_arr = cat(1, cur_split{:});
g_ids_comb = cat(1, g_ids{:});
[p_anova_corr, ~, stats] = anova1(cur_split_arr, g_ids_comb,'off');
[c, ~, ~, gnames] = multcompare(stats, 'display', 'off');
fprintf('Tha p-value anova for taste correlation: %d\n', p_anova_corr);

% Get row names from taste quality
taste_qs = gnames(c(:,1:2));
taste_q_row_names = arrayfun(@(x) [stub_rows,'_',taste_qs{x,1},'_',taste_qs{x,2}], ...
    1:size(taste_qs,1), 'UniformOutput', false);

pvals_T = table(repmat(p_anova_corr, size(c,1),1),...
    c(:,6), 'VariableNames', ...
    {'p_ANOVA', 'p_multcompare'}, 'RowNames', taste_q_row_names);

end

