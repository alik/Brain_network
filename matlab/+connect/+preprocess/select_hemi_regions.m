function [regions_T] = select_hemi_regions(T, region_names, hemisphere)
%SELECT_REGIONS select a set of regions within a hemisphere in the beta
%data table
% INPUT:
%   T: the beta data table with region_names and hemisphere information
%   region_names: a cell string array with the name and order of ROIs
%   hemisphere: the hemisphere
% OUTPUT:
%   updated_T
if ~exist('region_names','var') || isempty(region_names)
    region_names = unique(T.region_name);
end

if ~exist('hemisphere','var') || isempty(hemisphere)
    hemisphere = unique(T.hemisphere);
end
idx = ismember(T.hemisphere, hemisphere) & ...
    ismember(T.region_name,region_names);
regions_T = T(idx, :);
end

