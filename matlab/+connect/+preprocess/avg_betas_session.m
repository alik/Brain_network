function [beta_tbl] = avg_betas_session(beta_tbl)
%AVG_BETAS_SESSION add averages to the beta table per session
arr_vars_to_avg = {'rinse_Betas', 'high_low_betas'};
for i=1:length(arr_vars_to_avg)
    cur_var = arr_vars_to_avg{i};
    beta_tbl.([cur_var, '_avg']) = mean(beta_tbl.(cur_var),2);
end
cell_vars_to_avg = {'low_Betas', 'high_Betas'};
for i=1:length(cell_vars_to_avg)
    cur_var_cell = cell_vars_to_avg{i};
    beta_tbl.([cur_var_cell, '_avg']) = ...
        cellfun(@mean, beta_tbl.(cur_var_cell));
end

beta_tbl.diff_taste_rinse = beta_tbl.high_low_betas_avg - ...
    beta_tbl.rinse_Betas_avg;
end

