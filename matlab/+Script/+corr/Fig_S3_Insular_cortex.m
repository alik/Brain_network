%% Setup
util.clearAll;
outdir = fullfile(util.get_paths().figs,'Suppl_Fig_3');
util.mkdir(outdir)
c = util.plot.getColors();
%% Get the correlations of insular regions with the different communities
beta_tables_taste = util.load.all_taste_betas();
taste_names = beta_tables_taste.sep_table.Properties.VariableNames;
% Note: Correlations are between tastant (high_low_betas)
[avg_corr, correlations, corr_atanh] = connect.correlations(beta_tables_taste.combined, ...
    {{'L', 'R'}, {'L', 'R'}});
% Set the diagonal to nan
avg_corr = avg_corr.full;
avg_corr_arr = avg_corr.Variables;
avg_corr_arr(eye(size(avg_corr_arr))==1) = nan;
avg_corr.Variables = avg_corr_arr;

% Get the bilateral insular regions
insular_names={'Idfm_custom_bin','Idys','agranularIns_bin',...
    'dAIC_custom_bin_agranTrim','paraIns'};
bilateral_insular = cellfun(@(x) {['L_',x],['R_', x]}, insular_names, ...
    'UniformOutput', false);
bilateral_insular = cat(2, bilateral_insular{:});
avg_corr_insular = avg_corr(bilateral_insular, :);

%% Get the modules and separate

[order_index, module_index] = connect.preprocess.sort_by_module...
    (avg_corr_insular.Properties.VariableNames);
avg_corr_insular = avg_corr_insular(:,order_index);
avg_corr_insular_arr = avg_corr_insular.Variables;
module_index.Variables = module_index.Variables+1;
module_index_arr = repmat(module_index.Variables, ...
    height(avg_corr_insular), 1);

fh = figure(); ax = gca();
boxplot(avg_corr_insular_arr(:), module_index_arr(:),...
    'Colors', util.plot.getColors().modules)
ylim([0,1])
util.plot.cosmeticsSave(fh, ax, 4, 4, outdir,...
    strjoin({'boxplot_insular_correlations_per_module','svg'}, '.'), ...
    'off','on', true);

%% Anova testing
[p_anova_corr,tbl,stats] = anova1(atanh(avg_corr_insular_arr(:)), module_index_arr(:));
disp(tanh(stats.means))
mult_comp = multcompare(stats);
fprintf('Tha p-value anova for taste correlation: %d\n',p_anova_corr)