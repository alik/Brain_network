util.clearAll;
beta = util.load.beta_table();

%% Find the order of session indices and reset it for each animal
% get unique rows of session_index and animal_date_idx
[C, ia, ic] = unique(beta(:,[4,2]), 'rows','sorted');
num_range = groupcounts(C.animal_idx);
range_numbers = arrayfun(@(x) linspace(1,x,x)', num_range,...
    'UniformOutput',false);
C.new_index = cat(1, range_numbers{:});
reset_session_index = C.new_index(ic);
beta = ...
    addvars(beta,reset_session_index,'Before','session_index');

%% write beta table to mat and csv files
outdir = fullfile(util.get_paths().data_versioned, 'betas');
final_beta_table = beta;
save(fullfile(outdir, 'full_beta_tbl.mat'), 'final_beta_table')
f_name = fullfile(outdir, 'full_beta_tbl.csv');
writetable(beta, f_name)