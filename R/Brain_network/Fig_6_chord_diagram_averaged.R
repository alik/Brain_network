library('circlize')

# Read connectivity data
main_dir <- '/Users/karimia/code/Brain_Network/matlab/figs/chord_modularity'
file_name <- "mod_averaged"
out_name <- "Modules_averaged"
full_csv_name <- file.path(main_dir, paste(file_name, '.csv',sep = ""))
T <- read.csv(full_csv_name)

# set the row names and remove the Row column
rownames(T) <- T[,1]
T[,1] <- NULL
# Convert matrix and make the matrix symmetric
mat <- as.matrix(T)
colnames(mat)<-rownames(mat)
# Color palette for Edges: red, white, blue
color_list = list(c(255, 255, 255), c(0, 0, 0))
color_breaks = sapply(color_list, function(x)
  rgb(x[1], x[2], x[3], maxColorValue=255))
col_fun = colorRamp2(c(0.45, 0.65), color_breaks)
# grid column colors
# read colors
colors_mat <- read.csv(file.path(main_dir,'colors_mod_averaged.csv'))
# set the row names and remove the Row column
rownames(colors_mat) <- colors_mat[,1]
colors_mat[,1] <- NULL
# convert to matrix
colors_mat <- as.matrix(colors_mat)
grid.col <- apply(colors_mat, 1, function(x) 
  rgb(x[1], x[2], x[3], maxColorValue=255))

# open SVG object to save
out_name <- file.path(main_dir, paste(out_name,'.pdf', sep = ""))
pdf(out_name)

# create a gap of size 5 between grid elements
gap_after <- c(1,1,1) *50
circos.par(gap.after = gap_after)
# Convert to data frame
df = data.frame(from = rep(rownames(mat), times = ncol(mat)),
                to = rep(colnames(mat), each = nrow(mat)),
                value = as.vector(mat),
                stringsAsFactors = FALSE)

df=df[c(1,2,3,5,6,9),]
chordDiagram(df,link.sort = TRUE, link.decreasing = TRUE,
             symmetric = FALSE, 
             col = col_fun,
             grid.col = grid.col,
             annotationTrack = "grid",
             preAllocateTracks = 1,
             self.link = 1)
# rotate labels for readability
circos.track(track.index = 1, panel.fun = function(x, y) {
  circos.text(CELL_META$xcenter, CELL_META$ylim[1], CELL_META$sector.index, 
              facing = "clockwise", niceFacing = TRUE, adj = c(0, 0.5))
}, bg.border = NA) # here set bg.border to NA is important
circos.clear()

dev.off()