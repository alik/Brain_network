from operator import itemgetter
from pathlib import Path
import pickle
import os
import random

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

import networkx as nx
import community as community_louvain
from sklearn.metrics import adjusted_rand_score
from brainNetwork.util import util, path


def detect_community(G, random_state=None, print_modularity=True, cur_index=None):
    """
    detect community and return the modularity and
    """
    cur_partition = community_louvain.best_partition(G, random_state=random_state)
    cur_modularity = community_louvain.modularity(cur_partition,G)
    if print_modularity:
        print(f"The modularity is: {cur_modularity}")
    # Make the dict into a data frame
    if (random_state is None) and (cur_index is not None):
        index_str = f'partition_{cur_index}'
    else:
        index_str = f'partition_{random_state}'
    cur_partition_pd = pd.DataFrame(cur_partition, index = [index_str])
    return cur_partition_pd, cur_modularity

def repeat_community(G, num_repeats=1000, save_dir=None, perform_repetition=False):
    """
    repeat the community detection or load the saved CSV
    """
    # Load the saved partition
    if not perform_repetition:
        assert save_dir is not None
        df_partitions = pd.read_csv(os.path.join(save_dir, 'partitions.csv'),index_col=0)
        df_modularities = pd.read_csv(os.path.join(save_dir, 'modularity.csv'),index_col=0)
        return df_partitions, df_modularities
    # Otherwise perform the randomly seeded repition
    np.random.seed(100)
    RNG_seeds = np.random.randint(0, 2**32 - 1, num_repeats)
    # Loop over the seeds
    modularities = []
    partitions = []
    for _, cur_seed in enumerate(RNG_seeds):
        cur_partition_pd, cur_modularity = detect_community(G,random_state=cur_seed)
        partitions.append(cur_partition_pd)
        modularities.append(cur_modularity)
    # Create the dataframe
    df_partitions = pd.concat(partitions)
    df_modularities = pd.DataFrame(modularities, columns=['modularity'], index=df_partitions.index)
    if save_dir is not None:
        df_partitions.to_csv(os.path.join(save_dir, 'partitions.csv'))
        df_modularities.to_csv(os.path.join(save_dir, 'modularity.csv'))
    # Write the data 
    return df_partitions, df_modularities

def measure_rand_index(df_partitions, tril=False):
    """
    Measure the rand index between rows of the data frame
    """
    df_partitions = df_partitions.T
    vf = np.vectorize(adjusted_rand_score, signature='(n),(n)->()')
    result = vf(df_partitions.values, df_partitions.values[:, None])
    df_result = pd.DataFrame(result, index = df_partitions.index, columns=df_partitions.index)

    # Get the lower triangle
    if tril:
        df_result = df_result.where(np.tril(np.ones(df_result.shape),k=-1).astype(np.bool))
        df_result = df_result.stack().reset_index()
        df_result.columns = ['Row_mod','Column_mod','ARI']
    return df_result

def measure_rand_df_pair(df_partition_1, df_partition_2):
    """
    Measure the rand index between pairs of data frames (from tastant and rinse)
    """
    ARI_list = []
    row_list = []
    col_list = []
    for column_1 in df_partition_1:
        for column_2 in df_partition_2:
            cur_ARI = adjusted_rand_score(df_partition_1[column_1], df_partition_2[column_2])
            ARI_list.append(cur_ARI)
            row_list.append(column_1)
            col_list.append(column_2)
        
    ARI_df = pd.DataFrame({'Row': row_list, 'Col':col_list, 'ARI': ARI_list})
    return ARI_df

def rand_shuffled(df_shuf_partitions, output_path=None, perform_calculation=True, num_samples = 10000):

    if not perform_calculation:
        assert output_path is not None
        shuf_ARI = pd.read_csv(os.path.join(output_path, 'shuffled_matrix_ARI.csv'),index_col=0)
        return shuf_ARI

    shape_shuf = np.asarray([df_shuf_partitions.shape[0], df_shuf_partitions.shape[0]])
    # Use lower triangle indices
    tril_indices = np.ravel_multi_index(np.where(util.get_tril_bool(shape_shuf)), dims=shape_shuf)
    # Select a random subset of the tril indices without replacement
    np.random.seed(101)
    linear_indices = np.random.choice(tril_indices, num_samples, replace=False)
    # Get their row and column index
    row_idx, col_idx = np.unravel_index(linear_indices, shape=shape_shuf)
    assert ((row_idx - col_idx) >0).all()
    # Get the adjusted rand index of each pair
    ARI_list = []
    for i in range(num_samples):
        print(f'Shuffled ARI, [{row_idx[i]},{col_idx[i]}] :{i}')
        cur_ARI = adjusted_rand_score(df_shuf_partitions.iloc[row_idx[i], :], df_shuf_partitions.iloc[col_idx[i], :])
        ARI_list.append(cur_ARI)
        
    shuf_ARI = pd.DataFrame({'Row': row_idx, 'Col':col_idx, 'ARI_shuffled': ARI_list})
    if output_path is not None:
        shuf_ARI.to_csv(os.path.join(output_path, 'shuffled_matrix_ARI.csv'))
    return shuf_ARI

def replace_indices(a, vals_old, vals_new):
    arr = np.empty(a.max() + 1, dtype=a.dtype)
    arr[vals_old] = vals_new
    return arr[a]

def consistent_cluster_id(partitions):
    """
    apply replace indices to all the clusters (only works for specific use now)
    """
    cluster_indices = partitions.apply(pd.unique, axis=1)#partitions.loc[:,['L_ACC_area24', 'L_Idfm_custom_bin', 'L_PAG']]
    # Create consistent indices
    for i in range(len(cluster_indices.index)):
        cur_order = cluster_indices.iloc[i]
        rep_order = np.asarray(range(len(cur_order)))
        partitions.iloc[i,:]= replace_indices(partitions.iloc[i,:].values, cur_order, rep_order)

    # Assert the orders are sequential
    cluster_idx_corrected = partitions.apply(pd.unique, axis=1)
    assert cluster_idx_corrected.apply(lambda x: (x == np.asarray(range(len(x)))).all()).all()
    return partitions


def permute_modularity(pre_post_list, output_path=None, num_permute=10000, perform_repetition=True):
    """
    Get modularity from permutations of the source and target regions of the connectivity matrix
    """
    # Load the saved partition
    if not perform_repetition:
        assert output_path is not None
        df_partitions = pd.read_csv(os.path.join(output_path, 'partitions.csv'),index_col=0)
        df_modularities = pd.read_csv(os.path.join(output_path, 'modularity.csv'),index_col=0)
        return df_partitions, df_modularities
    
    modularities = []
    pre_post_permute = pre_post_list.copy()
    partitions = []
    for i in range(num_permute):
        # Permute the weights
        pre_post_permute['weight'] = np.random.permutation(pre_post_list.weight.values)
        # Get graph
        G = nx.from_pandas_edgelist(pre_post_permute, edge_attr=True)

        # Community detection
        cur_partition_pd, cur_modularity = detect_community(G, print_modularity=False, cur_index=i)
        
        # plot matrix
        debug = False
        if debug:
            # Plot the matrix
            plot_matrix(G=G, partition_pd=cur_partition_pd)
        print(f"The {i:05d}th modularity is: {cur_modularity}")

        # collect results
        partitions.append(cur_partition_pd)
        modularities.append(cur_modularity)

    # Create the dataframe
    df_partitions = pd.concat(partitions)
    df_modularities = pd.DataFrame(modularities, columns=['modularity'], index=df_partitions.index)
    if output_path is not None:
        df_partitions.to_csv(os.path.join(output_path, 'partitions.csv'))
        df_modularities.to_csv(os.path.join(output_path, 'modularity.csv'))

    return df_partitions, df_modularities

def read_permuted_modularity(path_modularity_file):
    """
    Read the pickled modularity file
    """
        # Load the modularities
    with open(path_modularity_file.joinpath('modularities_perm.pkl'), "rb") as fp:
        perm_modularities = pickle.load(fp)

    return perm_modularities

def plot_histogram(df_real_modularity, path_modularity_file, scatter_colors, plot_color = [0.5,0.5,0.5],save_plot=True):
    """
    Plot the histogram of the modularities
    """
    # Load the modularities
    with open(path_modularity_file.joinpath('modularities_perm.pkl'), "rb") as fp:
        perm_modularities = pickle.load(fp)

    plt.hist(perm_modularities, bins=100, color=plot_color, orientation='vertical')
    for idx, row in enumerate(df_real_modularity.iterrows()):
        cur_mod = row[1].modularities
        plt.scatter(x=cur_mod, y=np.repeat(row[1].ypos, len(cur_mod)), 
                    color=scatter_colors[idx, :], s=1, marker='x',linewidths=0.5)
    
    # Fix the axis tick labels
    # Get the limits and round according to the range
    if save_plot:
        #plt.gca().xaxis.tick_top()
        lims = {'X': None, 'Y': None}
        precisions = {'X': 2, 'Y': -2}
        lims['X'] = plt.xlim()
        lims['Y'] = plt.ylim()
        for cur_ax in lims.keys():
            lims[cur_ax] = [floor_prec(lims[cur_ax][0], precision=precisions[cur_ax]),
                            ceil_prec(lims[cur_ax][1], precision=precisions[cur_ax])] 
        # Have the same Y axis range
        lims['X'][1] = lims['X'][0] + 0.04
        # Set the limits
        plt.xlim(lims['X'])
        plt.ylim(lims['Y'])
        # Set the tick labels
        plt.xticks(np.arange(lims['X'][0], lims['X'][1]+0.01, 0.01))
        plt.yticks(np.arange(lims['Y'][0], lims['Y'][1]+1, 100))
        util.remove_box(plt.gca())
        plt.savefig(path_modularity_file.joinpath('modularities_perm.svg'))


def ceil_prec(a, precision=0):
    return np.true_divide(np.ceil(a * 10**precision), 10**precision)

def floor_prec(a, precision=0):
    return np.true_divide(np.floor(a * 10**precision), 10**precision)

def plot_matrix(G, partition_pd, save_dir=None):
    reordered_partitions = partition_pd.transpose().sort_values('partition_None')
    ordered_names = reordered_partitions.index
    connect_mat = nx.to_numpy_matrix(G,nodelist=ordered_names)
    # Plot the 
    fig = plt.figure(figsize=(7, 7))
    plt.imshow(connect_mat, cmap='gray_r', interpolation='none', vmin = 0, vmax=1)
    num_rois = connect_mat.shape[0]
    plt.xticks(np.arange(num_rois), ordered_names,rotation=90)
    plt.yticks(np.arange(num_rois), ordered_names,rotation=0)
    if save_dir is not None:
        fig_name = Path(save_dir, 'theMatrix.svg')
        fig.savefig(fig_name)


def get_graph_from_csv(f_name):
    # Read the matrix
    connect_mat = pd.read_csv(f_name, index_col=0)
    # Get the graph
    G = nx.convert_matrix.from_pandas_adjacency(connect_mat)
    return G

def get_unique_partitions(df_partitions, modularities, region_order=None):
    # Make sure the same modularity means the same clustering
    [vals, idx, counts] = np.unique(modularities.values, return_index=True, return_counts=True)
    for val in vals: 
        cur_same_mod = df_partitions.loc[modularities.values == val,:]
        assert cur_same_mod.eq(cur_same_mod.iloc[0, :], axis=1).all().all()

    # Measure randomness-adjusted rand score between unique partitions
    unique_partitions = df_partitions.iloc[idx,:]
    
    # turn index into frequency
    fractions = (counts*100) / counts.sum()
    unique_partitions.index = fractions
    
    # fractions and their modularities and sorted
    frac_modularities = pd.DataFrame((fractions, vals), index = ['fractions', 'modularities']).transpose()
    frac_modularities = frac_modularities.sort_values(by='modularities')

    # sort based on the last modularity
    unique_partitions = unique_partitions.T
    if region_order is None:
        unique_partitions = unique_partitions.sort_index(kind='stable')
    else:
        unique_partitions = unique_partitions.reindex(region_order)
    
    unique_partitions = unique_partitions.sort_values(by=frac_modularities['fractions'].iloc[-1], axis=0, kind='stable')
    return unique_partitions, frac_modularities


def plot_consistency(unique_partitions, fig_name=None):
    # Plot the consistency plot
    fig, ax = plt.subplots(1, 1, figsize=[4, 8])
    c_map_mods = util.colors()

    shape_part = unique_partitions.shape

    plt.imshow(c_map_mods[unique_partitions.values])
    plt.yticks(range(shape_part[0]), unique_partitions.index)
    plt.xticks(range(shape_part[1]), unique_partitions.columns, rotation=90)
    
    if fig_name is not None:
        fig.savefig(fig_name)

def get_per_taste_folder(f_name=None, sol_str=['tastant', 'rinse'], taste_str = ['salt', 'sour', 'sweet', 'combined']):
    """
    Read one file from all the subfolders of the different tatant and rinse
    """
    csv_main = Path(path.get_local_datadir(),'connectivity_matrices','repeat_modularity')

    paths = []
    for cur_s in sol_str:
        if f_name is None:
            paths.extend([csv_main.joinpath(f'connectivity_{cur_s}_{cur_t}') for cur_t in taste_str])
        else:
            paths.extend([csv_main.joinpath(f'connectivity_{cur_s}_{cur_t}', f_name) for cur_t in taste_str])

    df_paths = pd.DataFrame({'taste': taste_str*len(sol_str), 'solution': np.repeat(sol_str, len(taste_str)), 'paths':paths})
    return df_paths