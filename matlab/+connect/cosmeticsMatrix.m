function [hStrings] = cosmeticsMatrix(corr_matrix, colorLim, ...
    matrix_type, plot_textStrings)
% cosmeticsMatrix set the color map (pos: black, neg: red) and add the
% correlations as text strings
% INPUT:
%   corr_matrix: the correlation matrix
%   colorLim: 1 x 2 array, the limits for minimum and maximum of color map
%   matrix_type: 'tril' vs 'full' matrix type
%   plot_textStrings: boolean whether to plot text strings
% OUTPUT:
%   hStrings: the text string objects
% Colormap
arguments
    corr_matrix 
    colorLim
    matrix_type 
    plot_textStrings = true
end
data = corr_matrix(:);
L = length(data);
% Calculate where proportionally indexValue lies between minimum and
% maximum values
largest = max(colorLim);
smallest = min(colorLim);
indexValue = 0;     % value for which to set a particular color
index = L * max([indexValue - smallest, 0])/(largest - smallest);
% Color Edges
topColor = [0 0 0];         % color for maximum data value (red = [1 0 0])
indexColor = [1 1 1];       % color for indexed data value (white = [1 1 1])
bottomcolor = [165, 33, 32]./255; % color for minimum data value (blue = [0 0 1])

% Create color map ranging from bottom color to index color
% Multipling number of points by 100 adds more resolution
num = 100;
customCMap1 = [linspace(bottomcolor(1),indexColor(1),num*index)',...
    linspace(bottomcolor(2),indexColor(2),num*index)',...
    linspace(bottomcolor(3),indexColor(3),num*index)'];
% Create color map ranging from index color to top color
% Multipling number of points by 100 adds more resolution
customCMap2 = [linspace(indexColor(1),topColor(1),num*(L-index))',...
    linspace(indexColor(2),topColor(2),num*(L-index))',...
    linspace(indexColor(3),topColor(3),num*(L-index))'];
customCMap = [customCMap1;customCMap2];  % Combine colormaps
colormap(customCMap)
colorbar;
% The text in the figure

textStrings = num2str(corr_matrix(:), '%0.2f');       % Create strings from the matrix values
textStrings = strtrim(cellstr(textStrings));  % Remove any space padding

[x, y] = meshgrid(1:size(corr_matrix,2),...
    1:size(corr_matrix,1));  % Create x and y coordinates for the strings
x = x(:);
y = y(:);
textStrings = textStrings(:);
midValue = largest/2;  % Get the middle value of the color range
textColors = repmat(data > midValue, 1, 3);

if strcmp(matrix_type, 'tril')
    indices = find(~isnan(data));
    x = x(indices);
    y = y(indices);
    textStrings = textStrings(indices);
    textColors = textColors(indices, :);
end

if plot_textStrings
    hStrings = text(x, y, textStrings, ...  % Plot the strings
        'HorizontalAlignment', 'center');
    
    set(hStrings, {'Color'}, num2cell(textColors, 2));  % Change the text colors% Figure properties
    set(hStrings,'FontSize',6);
end
end

