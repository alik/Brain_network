function [dir_T] = get_file_table(path)
%GET_FILE_TABLE 
dir_info = dir(fullfile(path,'*.nii*'));
dir_info = struct2table(dir_info);
dir_T = table(dir_info);
end

