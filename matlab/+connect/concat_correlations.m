function [concat_corr] = concat_correlations(correlations, indices)
%CONCATENATE_CORRELATIONS concatenate correlations
correlations = correlations.Variables;
concat_corr = cat(1,correlations{indices});
end

