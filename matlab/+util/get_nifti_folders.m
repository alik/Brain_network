function [nifti_paths] = get_nifti_folders()
%GET_NIFTI_FOLDERS Get the directory of nifti folders
sub_1_dir = {'subcortical', 'cortical'};
sub_2_dir = {'bilateral'}; % {'left_hem', 'bilateral'}
nifti_paths = cell2table(cell(length(sub_1_dir), length(sub_2_dir)),...
    'RowNames', sub_1_dir, 'VariableNames', sub_2_dir);
for i = 1:length(sub_1_dir)
    for j = 1:length(sub_2_dir)
        nifti_paths{i,j}{1} = fullfile(util.get_data_dir(),'connectome_nodes', ...
            sub_1_dir{i}, sub_2_dir{j});
    end
end
end

