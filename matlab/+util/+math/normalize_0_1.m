function [normalize_arr] = normalize_0_1(arr, DIM)
%NORMALIZE_0_1 normalize to 0_1 range
arguments
    arr
    DIM = 1
end
normalize_arr = (arr - min(arr, [], DIM)) ./ (max(arr, [], DIM) - min(arr, [], DIM));
end

