function [gradient_c] = colors_modules()
%COLOR_SHADE Create the color map for regions involved in the modules 

% get colors and module names for each module
regions = util.str.chord_ROIs().modules;
colors = util.plot.getColors().modules;
gradient_c = cell(size(regions));

% Color gradient for each module
for i = 1:length(regions)
    max_c = colors(i, :);
    min_c = max_c ./ 1.5;
    gradient_c{i} = ...
        util.plot.get_color_gradient(min_c, max_c, length(regions{i}));
end
end

