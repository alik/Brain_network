function [h, p, ci, stat] = ttest(correlations, matrix_type)
%TTEST ttesting that the correlations are significantly different from zero
% with Bonferroni correction
arguments
    correlations (:, :, :)
    matrix_type string = 'tril'
end
% get the n
matrix_size = size(correlations, [1,2]);
switch matrix_type
    case 'full'
        numComparisons = prod(matrix_size);
    case 'tril'
        numComparisons = (matrix_size(1) .* (matrix_size(2)-1)) ./ 2;
end
corr_atanh = atanh(correlations);
alpha = 0.05/numComparisons;
corr_cell = num2cell(corr_atanh, 3);
% compute ttest of group-level means
[h, p, ci, stat] = cellfun(@(x) ttest(squeeze(x), 0, 'Alpha', alpha), ...
    corr_cell, 'UniformOutput', false);
end

