function [avg_corr] = write_matrix_from_table(beta_tbl, ...
    var_string, f_name)
%WRITE_MATRIX_FROM_TABLE write table from table of beta values
avg_corr = connect.correlations(beta_tbl, ...
    {{'L', 'R'}, {'L', 'R'}}, [], var_string);
avg_corr = avg_corr.full;
% necessary for the community detection that matrices be symmetric
avg_corr.Variables = util.math.make_symmetric(avg_corr.Variables);
writetable(avg_corr, f_name, 'WriteRowNames', true);
fprintf('File written to: %s\n', f_name)
end

