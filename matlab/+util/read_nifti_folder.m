function [volumes] = read_nifti_folder(nifti_path, hemisphere)
%READ_NIFTI_FOLDER read nifti files within a folder

% get .nii and .nii.gz files
nifti_dir = dir(fullfile(nifti_path, '*.nii*'));
nifti_dir_T = struct2table(nifti_dir);
row_names = cellfun(@(x) extractBefore(x, '.nii'),...
    nifti_dir_T.name, 'UniformOutput', false);
volumes = cell2table(cell(size(nifti_dir)), ...
    'RowNames', row_names, 'VariableNames', {'volume'});

% read the nifti files in the folder and binarize
for i = 1:height(nifti_dir_T)
    cur_dir = nifti_dir_T(i,:);
    cur_file = fullfile(cur_dir.folder, cur_dir.name);
    % read and binarize
    cur_vol = niftiread(cur_file{1}) > 0;
    % flip dim 1 and 2 so that first dim is X coordinates
    cur_vol = permute(cur_vol, [2, 1, 3]);
    % limit to selected hemisphere
    cur_vol = util.sub_volume(cur_vol, hemisphere);
    volumes{i, 1}{1} = cur_vol;
end

end

