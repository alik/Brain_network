%% Setup
% This script combines sessions from different tastes to make a combined
% connectivity matrix between the hemispheres
util.clearAll;
% stimuli
outdir = fullfile(util.get_paths().figs,'combine_tastes');
util.mkdir(outdir)

%% load the data and combine betas
% Get the beta tables from matfiles
taste_str = util.str.taste_strings();
beta_tables_taste = ...
    cellfun(@connect.read_matfiles_taste, taste_str, ...
    'UniformOutput', false);
combined_betas = cat(1, beta_tables_taste{:});

%% Get the correlations
matrix_type = 'full';
hemispheres = {'L', 'R'};
cur_name = 'Combined-taste-matrix';
% Correlations: get the average correlations
[avg_corr, correlations] = ...
    connect.correlations(combined_betas, hemispheres,'region_name');
% ttest for significant correlations
[ttest_results, ttest_p] = ...
    connect.ttest(correlations, matrix_type);
assert(all(cell2mat(ttest_results), 'all'));
% correlogram
fh = figure('Name',['Connectivity Matrix: ',...
    cur_name],...
    'Position', [10 10 800 800]);
ax = gca;
title(cur_name)
cur_save_name = fullfile(outdir, strjoin({cur_name,'pdf'}, '.'));
connect.plot_matrix(avg_corr.(matrix_type), matrix_type, true, [0.2, 1]);
util.plot.box_ticks(ax)
set(ax,'fontname', 'Arial')
exportgraphics(fh, cur_save_name, 'ContentType','vector');

%% Get the ipsilateral connections
matrix_type_ipsi = 'tril';
hemispheres_ipsi = {{'L', 'L'}, {'R', 'R'}};
correlations_ipsi = cell(size(hemispheres_ipsi));
correlations_ipsi_linear = cell(size(hemispheres_ipsi));
avg_ipsi = cell(size(hemispheres_ipsi));
% Correlations: get the average correlations
for i = 1:length(hemispheres_ipsi)
    % get the ipsilateral arrays
    [cur_avg_corr_ipsi, cur_corr_ipsi] = ...
        connect.correlations(combined_betas, hemispheres_ipsi{i},'region_name');
    avg_array = cur_avg_corr_ipsi.tril.Variables;
    avg_array_lin = avg_array(~isnan(avg_array));
    avg_ipsi{i} = avg_array_lin;
    cur_corr_ipsi = ...
        arrayfun(@(i) util.math.tril(cur_corr_ipsi(:,:,i),nan), ...
        1:size(cur_corr_ipsi,3), 'UniformOutput', false);
    correlations_ipsi{i} = cat(3, cur_corr_ipsi{:});
    correlations_ipsi_linear{i} = correlations_ipsi{i}(...
        ~isnan(correlations_ipsi{i}));
end

%% Compare diagonal (callosal) and non-diagonal (non-callosal) 
% elements with a box plot
% get elements
fh = figure(); ax = gca;
corr_matrix = avg_corr.(matrix_type).Variables;
same_region = diag(corr_matrix);
other_region = corr_matrix(logical(1-eye(length(corr_matrix))));
c = util.plot.getColors();
colors = [0.67, 0.84, 0.9; 0, 0, 0.54; c.gray*0.5; 0.8, 0.8, 0];
tick_labels = {'Left', 'Right', 'Other regions', 'Same region'};
% plot
ipsi_contra_avg = util.padcat(avg_ipsi{1}, avg_ipsi{2}, ...
    other_region, same_region);
boxplot(ipsi_contra_avg,'Colors',colors,'Symbol', 'x');
util.plot.box_ticks(gca);
set(gca, 'xtick',1:4, 'xticklabel',tick_labels)
xlabel('Connection type');
ylabel('Correlation coefficient')
ylim([0,1])

util.plot.cosmeticsSave...
    (fh, ax, 2, 4, outdir,...
     'boxplot_callosal_average_over_sessions.svg', 'off','on', true);
 
%% Get the callosal connections for individual sessions as well
fh = figure(); ax = gca;
diag_index = logical(repmat(eye(size(correlations(:,:,1))),...
    [1 1 size(correlations,3)]));
non_diag_index = logical(1-diag_index);
same_region_sess = correlations(diag_index);
other_Region_sess = correlations(non_diag_index);

boxplot(util.padcat(correlations_ipsi_linear{1}, correlations_ipsi_linear{2},...
    other_Region_sess,same_region_sess), 'Colors',colors, 'Symbol', 'x');
util.plot.box_ticks(gca);
set(gca, 'xtick',1:4, 'xticklabel',tick_labels)
xlabel('Connection type');
ylabel('Correlation coefficient')
%ylim([0,1])
util.plot.cosmeticsSave...
    (fh, ax, 2, 4., outdir,...
     'boxplot_callosal_per_sess_atanh.svg', 'off','on', true);

%% ANOVA for the ipsi and contra
ipsi_contra_atanh = atanh(ipsi_contra_avg);
[p_anova, tbl, stats] = anova1(ipsi_contra_atanh);
c = multcompare(stats);

%% ttest callosal vs non-callosal
[h, p] = ttest2(atanh(same_region),atanh(other_region));
fprintf('P value on atanh transformed correlations callosal vs non-callosal: %d\n', p);
