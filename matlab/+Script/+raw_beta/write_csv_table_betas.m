% Write table of betas to CSV for further analysis in python
beta = util.load.beta_table();

%% Actual writing
f_name = fullfile(util.get_paths().data_versioned, 'betas', 'full_beta_tbl.csv');
writetable(beta, f_name)

%% Reread to see
T = readtable(f_name);