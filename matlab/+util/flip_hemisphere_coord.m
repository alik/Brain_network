function [coords_contra] = flip_hemisphere_coord(coords)
%FLIP_HEMISPHERE_COORD flip the X dimension (lateral flip)
coords_contra = coords;
coords_contra(:, 1) = -coords_contra(:,1);
end

