function [A] = make_symmetric(A)
% MAKE_SYMMETRIC make the connectivity matric symmetric
A = A - diag(diag(A));
A = tril(A, -1) + tril(A, -1)';
assert(issymmetric(A))
end

