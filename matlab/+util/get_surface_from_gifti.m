function [g_surf] = get_surface_from_gifti(f_name)
%GET_SURFACE_FROM_GIFTI read gifti file and convert it to a mesh structure
% Note: requires gifti package
g = gifti(f_name);
g_surf = struct('faces', g.faces, 'vertices', g.vertices);
end

