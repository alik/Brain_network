function [regions_T] = filter_region(T, region_names)
%FILTER_REGION Select a specific region
idx = ismember(T.region_name,region_names);
regions_T = T(idx, :);
end

