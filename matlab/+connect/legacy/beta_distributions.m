function beta_distributions(dataDir, rootexpnam, betaseries, num_cond)
%% beta_distributions
%
% INPUTS:
% dataDir - default directory by startup.m
% rootexpname - shared experiment name folder prefix (e.g., 'M1_' ...)
% betaseries - was the GLM a beta series GLM? input is binary
% num_cond - number indicating how many event types are in the GLM

%% Go to where you're data are
cd(fullfile(dataDir,'/results/taste/betaseries'))
% and pick them all up
expID = dir(strcat(rootexpnam, '*')); % would need to standardize

%% Get the calculated ROI data structure for each subject
num_rois = 30;
preptoPlot = cell(num_rois, num_cond);
names = cell(num_rois, num_cond);
for isubj = 1:length(expID)
    load(strcat(expID(isubj).name, '/2/marsbarOutputs/dataOutput.mat'))  % load in example data structure
    
    %% If beta series, get the beta indices
    if betaseries == 1
        % load anaobj
        load(fullfile(dataDir, '/Rodent_fMRI/reward_history_study/Basco_analysis/2', ['out_estimated_2.mat']));
        % retrieve SPM desigm from analysis object
        SPMfile    = fullfile(anaobj{1, isubj}.Ana{1, 1}.AnaDef.DataPath, '2/betaseries/SPM.mat');
        for i = 1:num_cond
            theidx = anaobj{1, isubj}.Ana{1,1}.AnaDef.RegCondVec{(i)}; % indices in beta-series for chosen condition(s)
            betaIdx{i} = {theidx};
        end
    else fprintf('**set beta index for GLMs that are not beta series**');
    end
    
    %% Prepare data structure
    for roi_index = 1:num_rois
        toPlot = eventData.Beta_Values{roi_index,1};
        % take just the betas of interest
        for index_cond = 1:num_cond
            indexedPlot = toPlot(betaIdx{index_cond}{:});
            % concatenate from different subjects within the same condition
            % and ROI
            preptoPlot{roi_index, index_cond} = ...
                [preptoPlot{roi_index, index_cond}; indexedPlot];
            names{roi_index, index_cond} = ['ROI-', num2str(roi_index), '-cond-', num2str(index_cond)];
            % Plot data in a histogram
            % histogram(preptoPlot)
        end
    end
end

% organize the data structure
lengthOfArrays = cellfun(@(x) length(x), preptoPlot, 'UniformOutput',false);
grouping_variable = cellfun(@(x, length) categorical(cellstr(repmat(x, length, 1))), ...
    names, lengthOfArrays, 'UniformOutput', false);

%% Plot box plot
figure()
H = boxplot(cat(1, preptoPlot{:}), cat(1, grouping_variable{:}), 'whisker',2,'OutlierSize',2,'Symbol','o');
xtickangle(-60)
% misc extras
% ylim([-25 25]);
% title(strc