from numpy import array_split
from scipy.stats import chisquare
import pandas as pd
from pathlib import Path
# read observed and expected frequency per taste
main_dir = '/Users/karimia/code/Brain_Network/matlab/figs/betas_taste_animal'
dirs = [Path(main_dir).joinpath(x+'.csv') for x in ['observed', 'expected']]
Ts = [pd.read_csv(dir,index_col=0) for dir in dirs]
arrays = [t.to_numpy() for t in Ts]
# Chi square test
chiq, p_vals = chisquare(f_obs=arrays[0], f_exp=arrays[1], axis=1)
significance = p_vals < (0.05)

# Create table and display
p_value_T = pd.DataFrame([chiq, p_vals, significance],columns=Ts[0].index,
                         index=['chi_test_statistic', 'p_vals_chi_square', 'significance_bonf']).T
p_value_T.to_csv(Path(main_dir,'chi_square_animal_taste.csv'))