function [data_one_taste] = add_taste_animal(data_one_taste,taste)
%add animal index and taste this is for the separated by taste beta tables
animal_idx_full = char(data_one_taste.animal_idx);
animal_idx = animal_idx_full(:,1:3);
data_one_taste.only_animal_idx = categorical(cellstr(animal_idx));
data_one_taste.tastant = ...
    repmat(categorical({taste}), length(animal_idx), 1);

end

