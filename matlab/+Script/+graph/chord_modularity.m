%% Setup
util.clearAll;
outdir = fullfile(util.get_paths().figs,'chord_modularity');
util.mkdir(outdir)
region_names = util.str.chord_ROIs().modules_tbl;
colors = util.plot.colors_modules();
%% Setup graph
% Read beta values and calculate their correlation
beta_tables_taste = util.load.all_taste_betas();
% Work wit the combined across tastes field
beta_tables_taste =  beta_tables_taste.combined;

%% select specific ROIs
regions_strings = cat(2,region_names{:});
beta_region = beta_tables_taste;

for i = 1:height(beta_tables_taste)
    % sort rows by their ROI
    beta_region{i, 'beta_table'} = cellfun(@(x) sortrows(x, 'region_name'),...
        beta_tables_taste{i, 'beta_table'}, 'UniformOutput', false);
    % Select the specific regions
    beta_region{i, 'beta_table'} = ...
        cellfun(@(x) connect.preprocess.filter_region(x, regions_strings),...
        beta_region{i, 'beta_table'}, 'UniformOutput', false);
    
end
%% Calculate correlations
hemis = {{'L', 'R'}, {'L', 'R'}};
[~, ~, corr_atanh] = connect.correlations(beta_region, hemis);

% break into combined groups for each ROI's left and right (ACC is two rois)
atanh_per_ROI = mat2cell(corr_atanh, [4,repmat(2, 1, 17)], [4,repmat(2, 1, 17)]);
fun_mean = @(x) tanh(mean(cell2mat(x.Variables), [1,2],'omitnan'));
corr_averaged = cellfun(fun_mean, atanh_per_ROI);

% Get the ROI names
all_names = corr_atanh.Properties.RowNames;
% keep only right side and then remove their R_ part
all_names = all_names(contains(all_names, "R_"));
all_names = cellfun(@(x) erase(x, 'R_'), all_names, 'UniformOutput', false);
all_names = all_names(2:end);
% Sort according to the modules
[~, I] = ismember(regions_strings(2:end), all_names);
all_names = all_names(I);

% reorganize connectivity matrix
corr_averaged = array2table(corr_averaged(I,I),'RowNames',all_names, ...
    'VariableNames', all_names);
%% plot
corr_averaged_tril = corr_averaged;
corr_averaged_tril.Variables = util.math.tril(corr_averaged_tril.Variables,nan); 
connect.plot_matrix(corr_averaged_tril, 'tril');
exportgraphics(gca, fullfile(outdir, 'modular_matrix.pdf'))


%% Save the CSV files
f_name = fullfile(outdir, 'combined_hemis.csv');
writetable(corr_averaged, f_name, 'WriteRowNames', true);

%% colors
colors = util.plot.colors_modules();
colors = cat(1, colors{:});
colors = round(colors*255);
% remove extra ACC color
colors = colors(2:end,:);
colors = table(colors, 'RowNames', all_names, 'VariableNames', {'RGB'});

writetable(colors, fullfile(outdir, 'colors.csv'), ...
    'WriteRowNames', true)