function [identifier] = get_table_identifier(T, row, col)
%GET_TABLE_IDENTIFIER an identifier for plot names from the variable and
%row name
identifier = strjoin([strsplit(T.Properties.RowNames{row},'_'), ...
        strsplit(T.Properties.VariableNames{col}, '_')], ' ');
end

