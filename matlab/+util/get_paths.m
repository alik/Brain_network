function [paths] = get_paths()
%GET_PATHS Summary of this function goes here
%   Detailed explanation goes here
paths.main_dir = '/Users/karimia/code/Brain_Network/';
paths.main_code_dir = '/Users/karimia/code/Brain_Network/matlab';
paths.figs = fullfile(paths.main_code_dir, 'figs');
paths.data_versioned = fullfile(paths.main_dir, 'data_versioned');

paths.data_not_versioned = ...
    '/Users/karimia/code/Brain_Network/brainnet_data/';
paths.python_datadir = '/Users/karimia/code/Brain_Network/python/data';
end

