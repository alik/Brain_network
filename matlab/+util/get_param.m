function [p] = get_param()
%GET_PARAM Summary of this function goes here
%   Detailed explanation goes here
p.voxel_size = [.25, .25, .25];
p.bbox = [zeros(1, 3); [256, 312, 200] .* p.voxel_size]';
p.origin_index = [128.5, 112, 33];
p.offset = [128.5, 112, 33] .* -p.voxel_size;
p.bbox_shifted = p.bbox + p.offset';
end

