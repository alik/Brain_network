function [patches] = display_multiple(iso_surfaces, colors)
%DISPLAY_MULTIPLE display multiple isosurfaces in one figure
% isosurfaces: Table or cell. With cell you have to provide the colors
% yourself
if ~exist('colors','var') || isempty(colors)
    region_info = util.get_region_info();
    regions = iso_surfaces.Properties.RowNames;
    colors = region_info(regions,:).RGB_normalized;
    iso_surfaces = iso_surfaces.Variables;
end

patches = cellfun(@(x, y) util.display_surface(x, y), iso_surfaces, ...
    num2cell(colors, 2));

% axis off
% add bounding box
% bbox = util.get_param().bbox';
% util.plotcube(bbox(2, :), bbox(1, :), 0);
% set view
end

