function Surfaces = make_sphere_surf(coords, diameter)
% Create sphere surfaces from an numeric array of coordinates
% Author: Ali Karimi <ali.karimi@brain.mpg.de>
num_spheres = size(coords, 1);
Surfaces = cell(num_spheres, 1);
for i=1:num_spheres
    % Get unit sphere
    [X, Y, Z] = sphere(30);
    cur_surf = surf2patch(X, Y, Z, 'triangles');
    % Scale and translate
    cur_surf.vertices = cur_surf.vertices .* (diameter / 2);
    cur_coord = coords(i, 1:3);
    cur_surf.vertices = cur_surf.vertices + repmat(cur_coord,...
        [size(cur_surf.vertices,1),1]);
    Surfaces{i} = cur_surf;
end

end