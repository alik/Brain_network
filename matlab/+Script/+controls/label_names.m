function [x_labels] = label_names(taste_str,additional)
%LABEL_NAMES Get label names from taste and additional strings
[T, C] = ndgrid(taste_str, additional);
T = T'; T = T(:);
C = C'; C = C(:);
x_labels = cellfun(@(t,c)strcat(t,'-',c),T,C ,'UniformOutput',false);
end

