%% Setup
clear
close all
%% extract the surfaces from a nifti image of specific rois
hemisphere = 'left';
nifti_paths = util.get_nifti_folders();
volumes = util.create_empty_table(nifti_paths);
volumes.Variables = ...
    cellfun(@(x) util.read_nifti_folder(x, hemisphere), ...
    table2cell(nifti_paths), 'UniformOutput', false);

%% convert the images into isosurfaces
iso_surfaces = util.create_empty_table(nifti_paths);
iso_surfaces.Variables = cellfun(@util.extract_surface, ...
    table2cell(volumes), 'UniformOutput',false);

% combine cortical and subcortical surfaces
iso_surfaces{'cortex_and_sub',1} = {cat(1, ...
    iso_surfaces{{'cortical','subcortical'},1}{:})};

%% Plotting
% output directory
outdir = fullfile(util.get_paths().figs, 'ROI_isos', hemisphere);
util.mkdir(outdir);
% Plot the sub cortical and cortical separately
view_angles = {[-90, 0], [-180, 90], [-132, 13]};
project_type = {'orthographic','orthographic','perspective'};
for d = 1:height(iso_surfaces)
    % figure name: row and var names
    cur_name = util.get_table_identifier(iso_surfaces, d, 1);
    % Figure
    fh = figure('Name', cur_name);
    % ROI patches
    cur_patches = util.display_multiple(iso_surfaces{d, 1}{1});
    % Template
    util.add_template('rh.gray_surface.rsl.gii');
    % axes setting
    set(gca, 'TickDir','out')
    % Camera settings
    camproj(project_type{d});
    view(view_angles{d});
    camlight;
    lighting gouraud;
    % Save figure
    cur_save_name = fullfile(outdir, strjoin({cur_name,'png'}, '.'));
    print(cur_save_name, '-dpng', '-r300');
end
% Plot them together


%% colors

% low-res rh.gray_surface.inf_300.rsl.gii , lh.gray_surface.inf_300.rsl.gii 
% high-res lh.gray_surface.rsl.gii, rh.gray_surface.rsl.gii

% seeds: filtered_LR_Idfm_custom_bin.nii,
% filtered_LR_dAIC_custom_bin_agranTrim.nii, agranularIns.nii