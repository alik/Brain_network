function [beta_out] = merge_into_large_tbl(beta_table)
%MERGE_INTO_LARGE_TBL Create one large table from the beta_tables variable

% taste strings from Variable names
taste_strings = beta_table.Properties.VariableNames;
% Create a copy for merging
beta_out.merged = beta_table;
beta_out.single = beta_table;

for taste_idx = 1:length(taste_strings)
    % get the table of sessions of a single taste
    taste_str = taste_strings{taste_idx};
    cur_taste = beta_table{1,taste_idx}{1};
    for sess_idx = 1:height(cur_taste)
        % Get the session information
        cur_sess_T = cur_taste(sess_idx, 1:2);
        cur_sess_T.Properties.RowNames = {};
        % Get the session Roi
        cur_roi_T = cur_taste{sess_idx, 3}{1};
        cur_roi_T.Properties.RowNames = {};
        num_rois = height(cur_roi_T);
        % Expand taste, animal and session idx to the number of rois
        taste = repmat(categorical({taste_str}), num_rois, 1);
        taste = table(taste);
        cur_sess_T = repmat(cur_sess_T, num_rois, 1);
        % merge all information
        cur_merged_T = [taste, cur_sess_T, cur_roi_T];
        % save to the beta table variable of beta_table_merged
        beta_out.merged{1, taste_idx}{1}{sess_idx, 'beta_table'}{1} = ...
            cur_merged_T;
    end
    % Merge the beta_tables into a single table
    cur_taste_merge = beta_out.merged{1, taste_idx}{1}{:,'beta_table'};
    cur_taste_merge = cat(1, cur_taste_merge{:});
    beta_out.single{1, taste_idx}{1} = cur_taste_merge;
end
end

