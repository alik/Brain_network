import os

import pandas as pd

import networkx as nx
import community as community_louvain

from brainNetwork.util.path import get_local_datadir

csv_dir = os.path.join(get_local_datadir(),'connectivity.csv')
connect_mat = pd.read_csv(csv_dir, index_col=0)
# Get the graph
G = nx.convert_matrix.from_pandas_adjacency(connect_mat)
# Read Partition
partition_louvian = pd.read_csv(os.path.join(get_local_datadir(), 'partition.csv'),sep=';')
# convert to dictionary
partition_louvian = partition_louvian.to_dict('records')[0]
print(f"The modularity is: {community_louvain.modularity(partition_louvian,G)}")