% Goal: Do an anova test to find regions with correlations affected by the taste
%% Setup
util.clearAll;
cur_save_dir = fullfile(util.get_paths().figs,'ANOVA_correlations');
util.mkdir(cur_save_dir)
% Get the beta tables from matfiles
beta_tables_taste = util.load.all_taste_betas().sep;

%% get the Z transform of the correlations for the ANOVA testing
fun_corr = @(mat_T) connect.correlations(mat_T, {{'L', 'R'}, {'L', 'R'}});
[~, ~, corr_atanh] = ...
    cellfun(fun_corr, beta_tables_taste, 'UniformOutput', false);

% concatenate the same pair correlations for different tastes
corr_alltastes = util.create_empty_table(corr_atanh{1});
corr_alltastes.Variables = cellfun(@(x,y,z) util.padcat(x,y,z), ...
    corr_atanh{1}.Variables, corr_atanh{2}.Variables, ...
    corr_atanh{3}.Variables, 'UniformOutput', false);

% ANOVA test on each ROI pair
anova_corr_pvalue = array2table(zeros(size(corr_alltastes)),...
    'VariableNames', corr_alltastes.Properties.VariableNames,...
    'RowNames', corr_alltastes.Properties.RowNames);

% Get the p-values
[anova_corr_pvalue.Variables] = ...
    cellfun(@(x) anova1(x, [], 'off'), corr_alltastes.Variables);
by_module_idx = connect.preprocess.sort_by_module...
    (anova_corr_pvalue.Properties.RowNames);
anova_corr_pvalue = anova_corr_pvalue(by_module_idx, by_module_idx);
%% Plotting
fh = figure(); ax = gca;
% Set color limits, minimum: 4.8607e-05
color_Lim = [1e-5, 1.0];
anova_corr_pvalue.Variables = util.math.tril(...
    anova_corr_pvalue.Variables, nan);
im = imagesc(anova_corr_pvalue.Variables, color_Lim);
colormap hot
colorbar
% set log scale
set(ax, 'ColorScale', 'log')
util.plot.matrix_labels(anova_corr_pvalue)
util.plot.box_ticks(ax);
print(fullfile(cur_save_dir, 'ANOVA_pval_matrix.svg'), '-dsvg', '-r300');

%% False discovery rate adjustment of p-values:
% we use two methods offered by the matlab function mafdr:
% 1. Benjamini-hochberg 2. Storey et al

fh = figure(); ax = gca;
p_vals = anova_corr_pvalue.Variables;
p_vals_vector = sort(p_vals(~isnan(p_vals)));
% Get the FDR corrected p-values
% The benjamini-hochberg method of step-up alpha correction
[FDR_bh] = mafdr(p_vals_vector,'BHFDR', true);
% The Storey approach to get the q-values
[FDR_storey,Q,aPrioriProb] = mafdr(p_vals_vector,'BHFDR', false);
% Line plots
hold on
plot(p_vals_vector, FDR_bh,'Color', [0.5,0.5,0.5])
plot(p_vals_vector, Q,'Color', util.plot.getColors().red)
plot([1e-5, 1], [0.05, 0.05],'k:')
% figure settings
util.plot.box_ticks(ax);
util.plot.set_fig_size(fh, [5,5])
xlabel('Original p-values');
ylabel('FDR-adjusted p-values');
set(ax, 'XScale', 'log', 'FontSize', 8, 'fontname', 'Arial');

print(fullfile(cur_save_dir, 'ANOVA_FDR.svg'), '-dsvg', '-r300');